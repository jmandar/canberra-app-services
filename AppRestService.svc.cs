﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using KaamsAppRestService.KaamsNGCoreService;
using System.Data;
using System.IO;
using System.Web;
using System.Net;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Security.Cryptography;

namespace KaamsAppRestService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AppRestService" in code, svc and config file together.
    public class AppRestService : IAppRestService
    {
        public static string TOKEN = "ADTL14321";
        public const int Timeout = 5; // time out in minute
        private const string Token_Expired_Code = "103";
        private const string Token_Expired_Message = "Token Expired. Please login again to get new token.";
        private const string Token_NotProper_Code = "104";
        private const string Token_NotProper_Message = "Token Not Proper. Please login again to get valid token.";
        private const string No_Data_Code = "400";
        private const string No_Data_Message = "No Data";
        private const string User_Not_Found_Message = "Login Failed: Username or Password is not valid";
        private const string RequiredField_Message = "is required.";
        private const string Generic_Error_Message = "Generic Error.";
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];

        public static string sRegSpecialCharforGoal = "[^a-zA-Z0-9._!&-]";
        public static string sRegSpecialChar = "[^a-zA-Z0-9._@!&-]";
        public static string curPath = "";


        public Response<User> LoginUser(User requestLogin)
        {
            bool flag = false;
            string requiredMsg = "";

            if (requestLogin != null)
            {
                string username = requestLogin.UserName;
                string password = requestLogin.Password;
                string bundleID = requestLogin.BundleId;

                if (username == "")
                {
                    flag = true;
                    requiredMsg = "UserName";
                }
                else
                {
                    username = HttpUtility.UrlDecode(username, System.Text.Encoding.Default);
                }
                if (password == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", Password";
                }
                else
                {
                    password = HttpUtility.UrlDecode(password, System.Text.Encoding.Default);
                }
                if (bundleID == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", BundleID";
                }
                else
                {
                    bundleID = HttpUtility.UrlDecode(bundleID, System.Text.Encoding.Default);
                }

                if (!flag)
                {
                    try
                    {
                        ServiceKaamsNGClient client = new ServiceKaamsNGClient();
                        UserProfile uinfo = client.LoginUser(username, password, bundleID);

                        if (uinfo != null)
                        {
                            User u = new User();
                            u.EmailID = uinfo.emailId;
                            u.Location = uinfo.country;
                            u.ProfilePicture = uinfo.profilePicture;

                            u.UserFullName = uinfo.firstName + ' ' + uinfo.lastName;
                            u.UserID = Convert.ToString(uinfo.kaamsUserId);
                            // u.UserType = uinfo.UserType;
                            return new Response<User>(u, uinfo.Token);
                        }
                        else
                        {
                            return new Response<User>(ErrorCodes.USER_NOT_FOUND, User_Not_Found_Message, "");
                        }
                    }
                    catch (Exception ex)
                    {
                        return new Response<User>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, "");
                    }
                }
                else
                {
                    return new Response<User>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, "");
                }
            }
            else
            {
                return new Response<User>(ErrorCodes.USER_NOT_FOUND, User_Not_Found_Message, "");
            }
        }

        public Response<List<Library>> GetLibraryList(string userId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (userId != "")
                {

                    try
                    {
                        DTOLibrary[] libList = client.GetAllLibrariesWithTopicCountByUser(Convert.ToInt32(userId), 1);
                        List<Library> libraryList = new List<Library>();
                        if (libList.Count() > 0)
                        {
                            Library[] libTemp = new Library[libList.Count()];

                            foreach (DTOLibrary m in libList)
                            {
                                Library lnew = new Library();
                                lnew.LibraryID = Convert.ToString(m.LibraryId);
                                lnew.LibraryDetailID = Convert.ToString(m.LibraryDetailId);
                                lnew.LibraryName = m.LibraryTitle;
                                lnew.PreviewImage = m.Logo;
                                lnew.TopicsCount = Convert.ToString(m.TopicCount);
                                lnew.Description = m.Description;
                                lnew.DateCreated = Convert.ToString(m.CreatedDate);
                                libraryList.Add(lnew);
                            }


                            Response<List<Library>> response = new Response<List<Library>>(libraryList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<Library>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }
                    }
                    catch
                    {
                        return new Response<List<Library>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }

                }
                else
                {
                    return new Response<List<Library>>(ErrorCodes.REQUIREFIELD_ERROR, "UserId " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<Library>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<Library>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }


            // return strResult;
        }

        public Response<List<Topic>> GetLibraryData(string libId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (libId != "")
                {
                    try
                    {
                        int libraryid = Convert.ToInt32(libId);
                        DTOTopic[] tempList = client.GetAllTopicsInLibraryWithDetails(libraryid);

                        List<Topic> topicList = new List<Topic>();
                        if (tempList.Count() > 0)
                        {


                            foreach (DTOTopic m in tempList)
                            {
                                Topic tempTopic = new Topic();
                                tempTopic.TopicID = Convert.ToString(m.topicId);
                                tempTopic.TopicDetailID = Convert.ToString(m.topicDetailId);
                                tempTopic.TopicName = m.topicTitle;
                                tempTopic.PreviewImage = m.smallIcon;
                                tempTopic.ShortDescription = m.shortDescription;
                                tempTopic.LongDescription = m.longDescription.Trim();
                                tempTopic.DateCreated = Convert.ToString(m.CreatedDate);
                                tempTopic.SMEDetails = m.primarySME;
                                tempTopic.Likes = 5;
                                tempTopic.CommentsCount = m.CommentsCount;
                                tempTopic.LibTopicId = m.LibTopicId;
                                tempTopic.ArtefactCount = m.ArtefactCount;

                                topicList.Add(tempTopic);
                            }


                            Response<List<Topic>> response = new Response<List<Topic>>(topicList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<Topic>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }
                    }
                    catch
                    {
                        return new Response<List<Topic>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<Topic>>(ErrorCodes.REQUIREFIELD_ERROR, "LibraryID " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<Topic>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<Topic>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }

            // return strResult;
        }

        public Response<TopicWithArtefact> GetTopicData(string topicDetailId, string token)
        {

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (topicDetailId != "")
                {

                    try
                    {

                        KaamsNGCoreService.TopicDetailMst tempList = client.GetTopicDetailById(Convert.ToInt32(topicDetailId));//Convert.ToInt32(encryptId) 


                        if (tempList != null)
                        {

                            TopicWithArtefact tempTopic = new TopicWithArtefact();
                            tempTopic.TopicID = Convert.ToString(tempList.TopicMst.TopicId);
                            tempTopic.TopicDetailID = Convert.ToString(tempList.TopicDetailId);
                            tempTopic.TopicName = tempList.TopicTitle;
                            tempTopic.SMEDetails = tempList.PrimarySME;
                            tempTopic.ShortDescription = tempList.ShortDescription;
                            tempTopic.LongDescription = tempList.LongDescription.Trim();
                            tempTopic.PreviewImage = tempList.LargeIcon;
                            tempTopic.Owner = tempList.UserMst.KaamsUserId;
                            tempTopic.Status = tempList.StatusMst.Status;
                            tempTopic.Level = tempList.LevelMst.Level;
                            tempTopic.Objective = tempList.Objective;


                            int topDetailId = Convert.ToInt32(topicDetailId);
                            ArtefactListInfo[] artList = client.GetArtefactsFromTopic(topDetailId, true);
                            tempTopic.ArtefactCount = artList.Count();


                            List<Artefact> artefactList = new List<Artefact>();
                            if (artList.Count() > 0)
                            {


                                foreach (ArtefactListInfo m in artList)
                                {
                                    Artefact temp = new Artefact();
                                    temp.ArtifactID = Convert.ToString(m.ArtefactId);
                                    temp.ArtifactName = m.Title;
                                    temp.PreviewImage = m.largeIcon;
                                    temp.ArtefactTypeID = Convert.ToString(m.ArtefactTypeId);
                                    //temp.ArtefactType=m.a
                                    temp.CommentsCount = 0;
                                    temp.DateCreated = Convert.ToString(m.createdDate);
                                    temp.Description = m.Description;
                                    temp.Likes = 5;
                                    temp.SMEDetails = "";
                                    temp.ArtifactURL = m.filePath;

                                    artefactList.Add(temp);
                                }


                            }
                            tempTopic.ArtefactList = artefactList;
                            Response<TopicWithArtefact> response = new Response<TopicWithArtefact>(tempTopic, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<TopicWithArtefact>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }
                    }
                    catch
                    {
                        return new Response<TopicWithArtefact>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }

                }
                else
                {
                    return new Response<TopicWithArtefact>(ErrorCodes.REQUIREFIELD_ERROR, "TopicDetailID " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<TopicWithArtefact>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<TopicWithArtefact>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }


        }

        public Response<List<TopicWithArtefact>> GetLibraryTopicAndArtefactData(string libId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (libId != "")
                {
                    try
                    {
                        int libraryid = Convert.ToInt32(libId);
                        DTOTopic[] tempList = client.GetAllTopicsInLibraryWithDetails(libraryid);

                        List<TopicWithArtefact> topicList = new List<TopicWithArtefact>();
                        if (tempList.Count() > 0)
                        {


                            foreach (DTOTopic m in tempList)
                            {
                                TopicWithArtefact tempTopic = new TopicWithArtefact();
                                tempTopic.TopicID = Convert.ToString(m.topicId);
                                tempTopic.TopicDetailID = Convert.ToString(m.topicDetailId);
                                tempTopic.TopicName = m.topicTitle;
                                tempTopic.PreviewImage = m.smallIcon;
                                tempTopic.ShortDescription = m.shortDescription;
                                tempTopic.LongDescription = m.longDescription;
                                tempTopic.DateCreated = Convert.ToString(m.CreatedDate);
                                tempTopic.SMEDetails = m.primarySME;
                                tempTopic.Likes = m.Likes;//5 
                                tempTopic.CommentsCount = m.CommentsCount;
                                tempTopic.LibTopicId = m.LibTopicId;
                                tempTopic.ArtefactCount = m.ArtefactCount;
                                //added by maj:25/06/2014
                                tempTopic.Level = Convert.ToString(m.levelId);
                                tempTopic.ParentId = m.ParentId;
                                tempTopic.Type = m.TopicType;
                                //added by maj:25/06/2014
                                ArtefactListInfo[] tempArtList = client.GetArtefactsFromTopic(Convert.ToInt32(tempTopic.TopicDetailID), true);

                                List<Artefact> artefactList = new List<Artefact>();
                                if (tempList.Count() > 0)
                                {


                                    foreach (ArtefactListInfo a in tempArtList)
                                    {
                                        Artefact temp = new Artefact();
                                        temp.ArtifactID = Convert.ToString(a.ArtefactId);
                                        temp.ArtifactName = a.Title;
                                        temp.PreviewImage = a.largeIcon;
                                        temp.ArtefactTypeID = Convert.ToString(a.ArtefactTypeId);
                                        temp.ArtifactURL = a.filePath;
                                        temp.CommentsCount = 0;
                                        temp.DateCreated = Convert.ToString(a.createdDate);
                                        temp.Description = a.Description;
                                        temp.Likes = 5;
                                        temp.SMEDetails = "";
                                        artefactList.Add(temp);
                                    }

                                    tempTopic.ArtefactList = artefactList;
                                }
                                topicList.Add(tempTopic);
                            }


                            Response<List<TopicWithArtefact>> response = new Response<List<TopicWithArtefact>>(topicList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<TopicWithArtefact>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }
                    }
                    catch
                    {
                        return new Response<List<TopicWithArtefact>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<TopicWithArtefact>>(ErrorCodes.REQUIREFIELD_ERROR, "LibraryID " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<TopicWithArtefact>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<TopicWithArtefact>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }

        }

        public Response<List<Comments>> GetCommentsData(string topicid, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (topicid != "")
                {

                    try
                    {
                        int topID = Convert.ToInt32(topicid);
                        CommentMst[] tempCommentList = client.GetCommentsForTopic(topID, 0);
                        List<Comments> commentList = new List<Comments>();
                        if (tempCommentList.Count() > 0)
                        {


                            foreach (CommentMst m in tempCommentList)
                            {
                                Comments lnew = new Comments();
                                lnew.CommentID = Convert.ToString(m.CommentId);
                                lnew.CommentDetails = m.Comments;
                                lnew.CommentedBy = m.UserMst.Username;
                                lnew.TopicID = Convert.ToString(m.TopicMst.TopicId);
                                lnew.Time = Convert.ToString(m.ModifiedDate);

                                lnew.UserProfilePicture = Convert.ToString(m.UserMst.ProfilePicture);

                                commentList.Add(lnew);
                            }


                            Response<List<Comments>> response = new Response<List<Comments>>(commentList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {

                            return new Response<List<Comments>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);

                        }
                    }
                    catch
                    {
                        return new Response<List<Comments>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }

                }
                else
                {
                    return new Response<List<Comments>>(ErrorCodes.REQUIREFIELD_ERROR, "TopicID " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<Comments>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<Comments>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }

        }


        public Response<List<Comments>> PostComment(Request<Comments> comment)
        {

            Comments cmt = comment.Data;
            string token = comment.Token;
            string userid = cmt.UserID;
            string topicID = cmt.TopicID;
            string commentDetails = cmt.CommentDetails;
            string rating = cmt.Rating;


            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                bool flag = false;
                string requiredMsg = "";
                if (userid == null || userid == "")
                {
                    flag = true;
                    requiredMsg = "UserID";
                }
                if (topicID == null || topicID == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + " TopicID";
                }
                if (!flag)
                {
                    try
                    {
                        int usrID = Convert.ToInt32(userid);
                        int topID = Convert.ToInt32(topicID);

                        client.AddComments(usrID, commentDetails, Convert.ToInt32(rating), topID);
                        CommentMst[] tempCommentList = client.GetCommentsForTopic(Convert.ToInt32(topicID), 0);

                        List<Comments> commentList = new List<Comments>();
                        if (tempCommentList.Count() > 0)
                        {


                            foreach (CommentMst m in tempCommentList)
                            {
                                Comments lnew = new Comments();
                                lnew.CommentID = Convert.ToString(m.CommentId);
                                lnew.CommentDetails = m.Comments;
                                lnew.CommentedBy = m.UserMst.Username;
                                lnew.TopicID = Convert.ToString(m.TopicMst.TopicId);
                                lnew.Time = Convert.ToString(m.ModifiedDate);
                                lnew.UserProfilePicture = Convert.ToString(m.UserMst.ProfilePicture);
                                commentList.Add(lnew);
                            }


                            Response<List<Comments>> response = new Response<List<Comments>>(commentList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<Comments>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);

                        }
                    }
                    catch (Exception ex)
                    {
                        return new Response<List<Comments>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<Comments>>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<Comments>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<Comments>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }

        }

        public Response<List<Comments>> EditComment(Request<Comments> comment)
        {
            Comments cmt = comment.Data;
            string token = comment.Token;
            string commentId = cmt.CommentID;
            string commentDetails = cmt.CommentDetails;
            string rating = cmt.Rating;
            string topicid = cmt.TopicID;

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                bool flag = false;
                string requiredMsg = "";
                if (commentId == null || commentId == "")
                {
                    flag = true;
                    requiredMsg = "CommentID";
                }
                if (topicid == null || topicid == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + " TopicID";
                }
                if (!flag)
                {
                    try
                    {
                        int cmtID = Convert.ToInt32(commentId);
                        int topID = Convert.ToInt32(topicid);
                        client.EditComments(cmtID, commentDetails, Convert.ToInt32(rating));
                        CommentMst[] tempCommentList = client.GetCommentsForTopic(topID, 0);
                        List<Comments> commentList = new List<Comments>();
                        if (tempCommentList.Count() > 0)
                        {


                            foreach (CommentMst m in tempCommentList)
                            {
                                Comments lnew = new Comments();
                                lnew.CommentID = Convert.ToString(m.CommentId);
                                lnew.CommentDetails = m.Comments;
                                lnew.CommentedBy = m.UserMst.Username;
                                lnew.TopicID = Convert.ToString(m.TopicMst.TopicId);
                                lnew.Time = Convert.ToString(m.ModifiedDate);
                                lnew.UserProfilePicture = Convert.ToString(m.UserMst.ProfilePicture);
                                commentList.Add(lnew);
                            }

                            Response<List<Comments>> response = new Response<List<Comments>>(commentList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<Comments>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);

                        }
                    }
                    catch (Exception ex)
                    {
                        return new Response<List<Comments>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<Comments>>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<Comments>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<Comments>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }

        }

        public Response<List<Comments>> DeleteComment(Request<Comments> comment)
        {

            Comments cmt = comment.Data;
            string token = comment.Token;
            string commentId = cmt.CommentID;

            string topicid = cmt.TopicID;
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {

                bool flag = false;
                string requiredMsg = "";
                if (commentId == null || commentId == "")
                {
                    flag = true;
                    requiredMsg = "CommentID";
                }
                if (topicid == null || topicid == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + " TopicID";
                }
                if (!flag)
                {
                    try
                    {
                        int cmtID = Convert.ToInt32(commentId);
                        int topID = Convert.ToInt32(topicid);
                        client.DeleteComments(cmtID);
                        CommentMst[] tempCommentList = client.GetCommentsForTopic(topID, 0);
                        List<Comments> commentList = new List<Comments>();
                        if (tempCommentList.Count() > 0)
                        {


                            foreach (CommentMst m in tempCommentList)
                            {
                                Comments lnew = new Comments();
                                lnew.CommentID = Convert.ToString(m.CommentId);
                                lnew.CommentDetails = m.Comments;
                                lnew.CommentedBy = m.UserMst.Username;
                                lnew.TopicID = Convert.ToString(m.TopicMst.TopicId);
                                lnew.Time = Convert.ToString(m.ModifiedDate);
                                lnew.UserProfilePicture = Convert.ToString(m.UserMst.ProfilePicture);
                                commentList.Add(lnew);
                            }
                            Response<List<Comments>> response = new Response<List<Comments>>(commentList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {

                            return new Response<List<Comments>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);

                        }
                    }
                    catch (Exception ex)
                    {
                        return new Response<List<Comments>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<Comments>>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<Comments>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<Comments>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        public Response<Rating> PostRating(Request<Rating> ratingJson)
        {
            Rating cmt = ratingJson.Data;
            string token = ratingJson.Token;
            string topicId = cmt.TopicID;
            string rating = cmt.RatingNumber;
            string userid = cmt.UserID;
            string commentRatingId = cmt.CommentRatingID;
            string commentText = cmt.CommentText;
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {

                bool flag = false;
                string requiredMsg = "";
                if (userid == null || userid == "")
                {
                    flag = true;
                    requiredMsg = "UserID";
                }
                if (topicId == null || topicId == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + " TopicID";
                }
                if (topicId == null || rating == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + " Rating";
                }

                if (!flag)
                {
                    try
                    {
                        int topID = Convert.ToInt32(topicId);
                        int rat = Convert.ToInt32(rating);
                        int usID = Convert.ToInt32(userid);

                        //client.AddComments(Convert.ToInt32(userid), "", Convert.ToInt32(rating), Convert.ToInt32(topicId));
                        client.SaveRatingComment(commentText, Convert.ToInt32(commentRatingId), topID, usID, rat);

                        Rating r = new Rating();
                        r.AverageRating = Convert.ToString(client.GetAverageRating(Convert.ToInt32(topicId)));
                        r.TopicID = topicId;

                        Response<Rating> response = new Response<Rating>(r, validMessageOrNewToken);
                        return response;
                    }
                    catch (Exception ex)
                    {
                        return new Response<Rating>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<Rating>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<Rating>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<Rating>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }

        }

        public Response<List<Artefact>> CreateArtefact(Request<Artefact> artefact)
        {
            Artefact requestArtefact = artefact.Data;
            string token = artefact.Token;


            string topicDetailId = requestArtefact.TopicDetailID;
            string artefactTitle = requestArtefact.ArtifactName;
            string description = requestArtefact.Description;
            string ownerId = requestArtefact.OwnerID;

            string languageId = requestArtefact.LanguageID;
            string artefactStatusId = requestArtefact.ArtefactStatusID;
            string artefactTypeId = requestArtefact.ArtefactTypeID;
            string filename = requestArtefact.FileName;
            string version = requestArtefact.Version;
            string expiryDate = requestArtefact.ExpiryDate;
            string smallIcon = requestArtefact.SmallIcon;
            string largeIcon = requestArtefact.LargeIcon;
            string keywords = requestArtefact.KeyWords;
            string transcript = requestArtefact.Transcript;
            string createdBy = requestArtefact.CreatedBy;
            string ownerID = requestArtefact.OwnerID;
            string base64StringContent = requestArtefact.ArtefactContent;
            string contentType = requestArtefact.ContentType;

            string reviewDate = requestArtefact.ReviewDate;

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                bool flag = false;
                string requiredMsg = "";


                if (topicDetailId == null || topicDetailId == "")
                {
                    flag = true;
                    requiredMsg = "TopicDetailID";

                }
                if (createdBy == null || createdBy == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", CreatedBy";
                }
                else
                {
                    try
                    {
                        int tempCreatedBy = Convert.ToInt32(createdBy);
                    }
                    catch (Exception ex)
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", CreatedBy must be integer (id of user)";
                    }
                }
                //if (base64StringContent == null || base64StringContent == "")
                //{
                //    flag = true;
                //    requiredMsg = requiredMsg + ", ArtefactContent";
                //}
                if (artefactTypeId == null || artefactTypeId == "" || artefactTypeId == "0")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", ArtefactTypeID (It must have values between 1 to 8 for 1:Video, 2:Pdf, 3:Doc, 4:Ppt, 5:Xls, 6:Audio,7:Image,8:Text)";
                }
                if (filename == null || filename == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", FileName";
                }
                //else
                //{
                //    filename = GetFileNameHavingOriginalaName(filename);

                //}
                if (artefactStatusId == null || artefactStatusId == "" || artefactStatusId == "0")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", ArtefactStatusID (It must have values between 1 to 4)";
                }

                if (expiryDate == null || expiryDate == "")
                {
                    expiryDate = DateTime.Now.AddYears(20).ToShortDateString();
                }
                else
                {
                    try
                    {
                        DateTime tempDate = DateTime.ParseExact(expiryDate.Trim(), "d/M/yyyy", CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", Expiry date must be in format (dd/mm/yyyy)";

                    }
                }
                if (reviewDate == null || reviewDate == "")
                {
                    reviewDate = DateTime.Now.AddYears(20).ToShortDateString();
                }
                else
                {
                    try
                    {
                        DateTime tempDate = DateTime.ParseExact(reviewDate.Trim(), "d/M/yyyy", CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", Expiry date must be in format (dd/mm/yyyy)";

                    }
                }


                if (flag)
                {
                    return new Response<List<Artefact>>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                }
                else
                {
                    try
                    {
                        DTOArtefact dtoArtefact = new DTOArtefact();

                        dtoArtefact.ArtefactTitle = artefactTitle;
                        dtoArtefact.ArtefactTypeId = Convert.ToInt32(artefactTypeId);
                        //dtoArtefact.Base64ArtefactContent = base64StringContent;
                        dtoArtefact.CreatedBy = Convert.ToInt32(createdBy);
                        dtoArtefact.Description = description;
                        dtoArtefact.ExpiryDate = expiryDate;
                        dtoArtefact.FileName = filename;
                        dtoArtefact.Keywords = keywords;
                        dtoArtefact.LanguageId = 1;
                        dtoArtefact.LargeIcon = largeIcon;
                        dtoArtefact.SmallIcon = "";
                        dtoArtefact.OwnerId = Convert.ToInt32(ownerID);
                        dtoArtefact.TopicDetailID = Convert.ToInt32(topicDetailId);
                        dtoArtefact.Transcript = transcript;
                        dtoArtefact.Version = version;
                        dtoArtefact.ReviewDate = reviewDate;
                        // dtoArtefact.ArtefactStatusId = 1;


                        //string uploadResponse = PostFile(base64StringContent, filename, contentType);

                        //if (uploadResponse != null && uploadResponse.ToLower() == "true")
                        //{

                        dtoArtefact.ArtefactStatusId = Convert.ToInt32(artefactStatusId);
                        ArtefactListInfo[] tempArtList = client.UploadArtefact(dtoArtefact);

                        List<Artefact> arttList = new List<Artefact>();
                        if (tempArtList.Count() > 0)
                        {


                            foreach (ArtefactListInfo m in tempArtList)
                            {
                                Artefact temp = new Artefact();
                                temp.ArtifactID = Convert.ToString(m.ArtefactId);
                                temp.ArtifactName = m.Title;
                                temp.PreviewImage = m.largeIcon;
                                temp.ArtifactURL = m.filePath;
                                temp.CommentsCount = 0;
                                temp.DateCreated = Convert.ToString(m.createdDate);
                                temp.Description = m.Description;
                                temp.Likes = 5;
                                temp.SMEDetails = "";

                                arttList.Add(temp);
                            }


                            Response<List<Artefact>> response = new Response<List<Artefact>>(arttList, validMessageOrNewToken);
                            return response;

                        }
                        else
                        {
                            return new Response<List<Artefact>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);
                        }
                        //}
                        // else
                        //{
                        //return new Response<List<Artefact>>(ErrorCodes.UPLOAD_FAILED, "File upload Error", validMessageOrNewToken);
                        //}
                    }
                    catch (Exception ex)
                    {
                        return new Response<List<Artefact>>(ErrorCodes.GENERIC_ERROR, "Generic Error", validMessageOrNewToken);
                    }
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<Artefact>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<Artefact>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }


        }

        public Response<List<CashPoints>> GetCashPointsOfUser(string userId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (userId != null && userId != "")
                {
                    try
                    {
                        /*string result = client.GetKassPointsOfUser(Convert.ToInt32(userId));//Convert.ToInt32(encryptId) 

                        string[] s = { "$$" };
                        string[] sep = result.Split(s, StringSplitOptions.RemoveEmptyEntries);

                        CashPoints cashpoint = new CashPoints();
                        cashpoint.TotalCashPoint = sep[0];
                        cashpoint.UserFavorite = sep[2];
                        cashpoint.UserView = sep[9];
                        cashpoint.UserRecommend = sep[3];

                        cashpoint.UserFollow = sep[1];
                        cashpoint.OwnerCreate = sep[4];
                        cashpoint.OwnerView = sep[5];
                        cashpoint.OwnerFavorite = sep[7];
                        cashpoint.OwnerRecommend = sep[6];
                        cashpoint.OwnerFollow = sep[8];

                        Response<CashPoints> response = new Response<CashPoints>(cashpoint, validMessageOrNewToken);
                        return response;*/


                        List<KaamsNGCoreService.Kashpoints> plotData = new List<KaamsNGCoreService.Kashpoints>();
                        List<CashPoints> result = new List<CashPoints>();
                        plotData = new List<KaamsNGCoreService.Kashpoints>(client.GetKashPoints(Convert.ToInt32(userId)));
                        if (plotData.Count() > 0)
                        {
                            foreach (KaamsNGCoreService.Kashpoints m in plotData)
                            {
                                CashPoints tempCashPnt = new CashPoints();
                                tempCashPnt.Type = m.type;
                                tempCashPnt.Points = m.points;

                                result.Add(tempCashPnt);

                            }
                            Response<List<CashPoints>> response = new Response<List<CashPoints>>(result, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<CashPoints>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }


                    }
                    catch
                    {
                        return new Response<List<CashPoints>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<CashPoints>>(ErrorCodes.REQUIREFIELD_ERROR, "User Id " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<CashPoints>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<CashPoints>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        public Response<List<NotificationList>> GetUserNotification(string userId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (userId != null && userId != "")
                {
                    try
                    {
                        KaamsNGCoreService.NotificationList[] tempList = client.GetUserNotification(Convert.ToInt32(userId));//Convert.ToInt32(encryptId) 
                        List<NotificationList> notificationList = new List<NotificationList>();

                        if (tempList.Count() > 0)
                        {


                            foreach (KaamsNGCoreService.NotificationList m in tempList)
                            {
                                NotificationList tempNotification = new NotificationList();
                                tempNotification.Content = m.Content;
                                tempNotification.ContentTitle = m.ContentTitle;
                                tempNotification.Date = m.Date;
                                tempNotification.FromUser = m.FromUser;
                                tempNotification.Id = m.Id;
                                tempNotification.ProfilePicture = m.ProfilePicture;
                                tempNotification.RecommendedId = m.RecommendedId;
                                tempNotification.Title = m.Title;
                                tempNotification.ToUser = m.ToUser;
                                tempNotification.Viewed = m.Viewed;

                                notificationList.Add(tempNotification);
                            }


                            Response<List<NotificationList>> response = new Response<List<NotificationList>>(notificationList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<NotificationList>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<List<NotificationList>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<NotificationList>>(ErrorCodes.REQUIREFIELD_ERROR, "User Id " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<NotificationList>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<NotificationList>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        public Response<List<UserRecommendMst>> GetRecommendedListForUser(string userId, string languageId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                bool flag = false;
                string requiredMsg = "";


                if (userId == null && userId == "")
                {
                    flag = true;
                    requiredMsg = "UserId";

                }
                if (languageId == null || languageId == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", LanguageId";
                }

                if (!flag)
                {
                    try
                    {
                        KaamsNGCoreService.UserRecommendMst[] tempList = client.GetRecommendedListForHomePage(Convert.ToInt32(userId));//Convert.ToInt32(encryptId) 
                        List<UserRecommendMst> recommendationList = new List<UserRecommendMst>();

                        if (tempList.Count() > 0)
                        {


                            foreach (KaamsNGCoreService.UserRecommendMst m in tempList)
                            {
                                UserRecommendMst tempRecommend = new UserRecommendMst();
                                tempRecommend.RecommendId = m.RecommendedId;
                                tempRecommend.ContentTypeName = m.ContentType;
                                tempRecommend.ContentId = m.ContentID;

                                tempRecommend.RecommendedById = m.UserMst.KaamsUserId;
                                tempRecommend.RecommendedByName = m.UserMst.FirstName + " " + m.UserMst.LastName;
                                tempRecommend.RecommendedByProfilePicture = m.UserMst.ProfilePicture;

                                //tempRecommend.RecommendedToId = m.UserMst1.KaamsUserId;
                                //tempRecommend.RecommendedToName = m.UserMst1.FirstName + " " + m.UserMst1.LastName;
                                tempRecommend.Title = m.Title;
                                tempRecommend.Viewed = m.Viewed;


                                DTOTopic topicFromDb = client.GetTopicDetailByTopicIdByLanguage(Convert.ToInt32(m.ContentID), Convert.ToInt32(languageId));

                                if (topicFromDb != null)
                                {
                                    Topic tempTopic = new Topic();
                                    tempTopic.TopicID = Convert.ToString(m.ContentID);
                                    tempTopic.TopicDetailID = Convert.ToString(topicFromDb.topicDetailId);
                                    tempTopic.TopicName = topicFromDb.topicTitle;
                                    tempTopic.PreviewImage = topicFromDb.smallIcon;
                                    tempTopic.ShortDescription = topicFromDb.shortDescription;
                                    tempTopic.LongDescription = topicFromDb.longDescription.Trim();
                                    tempTopic.DateCreated = Convert.ToString(topicFromDb.CreatedDate);
                                    tempTopic.SMEDetails = topicFromDb.primarySME;
                                    tempTopic.Likes = 5;
                                    tempTopic.CommentsCount = topicFromDb.CommentsCount;
                                    tempTopic.LibTopicId = topicFromDb.LibTopicId;
                                    tempTopic.ArtefactCount = topicFromDb.ArtefactCount;
                                    tempRecommend.TopicDetails = tempTopic;
                                    tempTopic.LibraryId = topicFromDb.libraryId;//added by maj 30/07/2014
                                }

                                recommendationList.Add(tempRecommend);
                            }


                            Response<List<UserRecommendMst>> response = new Response<List<UserRecommendMst>>(recommendationList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<UserRecommendMst>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<List<UserRecommendMst>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<UserRecommendMst>>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<UserRecommendMst>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<UserRecommendMst>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        /// <summary>
        /// modified by maj 04/08/2014
        /// purpose:Get favourite topic list of user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="languageId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Response<List<FavoriteList>> GetUserFavorite(string userId, string languageId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                bool flag = false;
                string requiredMsg = "";


                if (userId == null && userId == "")
                {
                    flag = true;
                    requiredMsg = "UserId";

                }
                if (languageId == null || languageId == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", LanguageId";
                }

                if (!flag)
                {
                    try
                    {
                        KaamsNGCoreService.FavouritesMst[] tempList = client.GetFavouritesListForHomePage(Convert.ToInt32(userId));//Convert.ToInt32(encryptId) 
                        List<FavoriteList> favoriteList = new List<FavoriteList>();

                        if (tempList.Count() > 0)
                        {


                            foreach (KaamsNGCoreService.FavouritesMst m in tempList)
                            {
                                if (m.ContentId > 0)
                                {
                                    FavoriteList tempFavorite = new FavoriteList();
                                    tempFavorite.ContentId = m.ContentId;
                                    tempFavorite.ContentType = m.ContentType;
                                    tempFavorite.CreatedDate = m.CreatedDate;
                                    tempFavorite.ModifiedDate = m.ModifiedDate;
                                    tempFavorite.FavoriteId = m.FavouriteId;
                                    tempFavorite.URL = m.URL;
                                    tempFavorite.Title = m.Title;

                                    DTOTopic topicFromDb = client.GetTopicDetailByTopicIdByLanguage(Convert.ToInt32(m.ContentId), Convert.ToInt32(languageId));

                                    if (topicFromDb != null)
                                    {
                                        Topic tempTopic = new Topic();
                                        tempTopic.TopicID = Convert.ToString(m.ContentId);
                                        tempTopic.TopicDetailID = Convert.ToString(topicFromDb.topicDetailId);
                                        tempTopic.TopicName = topicFromDb.topicTitle;
                                        tempTopic.PreviewImage = topicFromDb.smallIcon;
                                        tempTopic.ShortDescription = topicFromDb.shortDescription;
                                        tempTopic.LongDescription = topicFromDb.longDescription.Trim();
                                        tempTopic.DateCreated = Convert.ToString(topicFromDb.CreatedDate);
                                        tempTopic.SMEDetails = topicFromDb.primarySME;
                                        tempTopic.Likes = 5;
                                        tempTopic.CommentsCount = topicFromDb.CommentsCount;
                                        tempTopic.LibTopicId = topicFromDb.LibTopicId;
                                        tempTopic.ArtefactCount = topicFromDb.ArtefactCount;
                                        tempFavorite.TopicDetails = tempTopic;
                                        tempTopic.LibraryId = topicFromDb.libraryId;//added by maj 30/07/2014
                                    }

                                    favoriteList.Add(tempFavorite);
                                }

                            }


                            Response<List<FavoriteList>> response = new Response<List<FavoriteList>>(favoriteList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<FavoriteList>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<List<FavoriteList>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<FavoriteList>>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<FavoriteList>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<FavoriteList>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        /// <summary>
        /// modified by maj 04/08/2014
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="languageId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Response<List<RecentViewList>> GetUserRecentView(string userId, string languageId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                bool flag = false;
                string requiredMsg = "";


                if (userId == null && userId == "")
                {
                    flag = true;
                    requiredMsg = "UserId";

                }
                if (languageId == null || languageId == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", LanguageId";
                }
                if (!flag)
                {
                    try
                    {
                        KaamsNGCoreService.MyRecentlyViewed[] tempList = client.GetRecentViewListForHomePage(Convert.ToInt32(userId));//Convert.ToInt32(encryptId) 
                        List<RecentViewList> favoriteList = new List<RecentViewList>();

                        if (tempList.Count() > 0)
                        {


                            foreach (KaamsNGCoreService.MyRecentlyViewed m in tempList)
                            {
                                RecentViewList tempFavorite = new RecentViewList();
                                tempFavorite.ContentId = m.ContentId;
                                tempFavorite.ContentType = m.ContentType;
                                tempFavorite.CreatedDate = m.CreatedDate;
                                tempFavorite.ModifiedDate = m.ModifiedDate;
                                tempFavorite.RecentViewId = m.RecentViewId;
                                tempFavorite.URL = m.URL;
                                tempFavorite.Title = m.Title;

                                DTOTopic topicFromDb = client.GetTopicDetailByTopicIdByLanguage(Convert.ToInt32(m.ContentId), Convert.ToInt32(languageId));

                                if (topicFromDb != null)
                                {
                                    Topic tempTopic = new Topic();
                                    tempTopic.TopicID = Convert.ToString(m.ContentId);
                                    tempTopic.TopicDetailID = Convert.ToString(topicFromDb.topicDetailId);
                                    tempTopic.TopicName = topicFromDb.topicTitle;
                                    tempTopic.PreviewImage = topicFromDb.smallIcon;
                                    tempTopic.ShortDescription = topicFromDb.shortDescription;
                                    tempTopic.LongDescription = topicFromDb.longDescription.Trim();
                                    tempTopic.DateCreated = Convert.ToString(topicFromDb.CreatedDate);
                                    tempTopic.SMEDetails = topicFromDb.primarySME;
                                    tempTopic.Likes = 5;
                                    tempTopic.CommentsCount = topicFromDb.CommentsCount;
                                    tempTopic.LibTopicId = topicFromDb.LibTopicId;
                                    tempTopic.ArtefactCount = topicFromDb.ArtefactCount;
                                    tempFavorite.TopicDetails = tempTopic;
                                    tempTopic.LibraryId = topicFromDb.libraryId;//added by maj 30/07/2014
                                }

                                favoriteList.Add(tempFavorite);

                            }


                            Response<List<RecentViewList>> response = new Response<List<RecentViewList>>(favoriteList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<RecentViewList>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<List<RecentViewList>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<RecentViewList>>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<RecentViewList>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<RecentViewList>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        public Response<List<TopicRelatedTopic>> GetTopicRelatedTopics(string topicDetailId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (topicDetailId != null && topicDetailId != "")
                {
                    try
                    {
                        //object changed from Related topic to RelatedTopicFront by maj - 19/06/2014
                        KaamsNGCoreService.RelatedTopicFront[] tempList = client.GetRelatedTopicsOfTopic(Convert.ToInt32(topicDetailId));//Convert.ToInt32(encryptId) 
                        List<TopicRelatedTopic> relatedTopicList = new List<TopicRelatedTopic>();

                        if (tempList.Count() > 0)
                        {


                            foreach (KaamsNGCoreService.RelatedTopicFront m in tempList)
                            {
                                TopicRelatedTopic tempRelatedTopic = new TopicRelatedTopic();
                                tempRelatedTopic.RelatedTopicsId = m.RelatedTopicId;
                                tempRelatedTopic.TopicDetailId = m.topicDetailId;
                                tempRelatedTopic.TopicName = m.title;
                                if (m.ModifiedDate != null)
                                {
                                    tempRelatedTopic.ModifiedDate = m.ModifiedDate.ToString("dd/MM/yyyy");
                                }
                                if (m.CreatedDate != null)
                                {
                                    tempRelatedTopic.CreatedDate = m.CreatedDate.ToString("dd/MM/yyyy");
                                }
                                tempRelatedTopic.ShortDescription = m.ShortDescription;
                                tempRelatedTopic.SMEDetails = m.SMEDetails;
                                tempRelatedTopic.LongDescription = m.LongDescription;
                                tempRelatedTopic.Objective = m.Objective;
                                tempRelatedTopic.Owner = m.Owner;
                                if (m.ReviewDate != null)
                                {
                                    tempRelatedTopic.ReviewDate = m.ReviewDate.ToString("dd/MM/yyyy");
                                }
                                if (m.ExpiryDate != null)
                                {
                                    tempRelatedTopic.ExpiryDate = m.ExpiryDate.ToString("dd/MM/yyyy");
                                }
                                tempRelatedTopic.Status = m.Status;
                                tempRelatedTopic.Level = Convert.ToString(m.Level);
                                //tempRelatedTopic.liberaryId = m.liberaryId;
                                tempRelatedTopic.SmallIcon = m.SmallIcon;
                                tempRelatedTopic.LargeIcon = m.LargeIcon;
                                relatedTopicList.Add(tempRelatedTopic);

                            }
                            Response<List<TopicRelatedTopic>> response = new Response<List<TopicRelatedTopic>>(relatedTopicList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<TopicRelatedTopic>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<List<TopicRelatedTopic>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<TopicRelatedTopic>>(ErrorCodes.REQUIREFIELD_ERROR, "TopicDetailId " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<TopicRelatedTopic>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<TopicRelatedTopic>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        /// <summary>
        /// modified by maj
        /// </summary>
        /// <param name="topicDetailId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Response<TopicSpecific> GetTopicInfoByTopicDetailId(string topicDetailId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (topicDetailId != null && topicDetailId != "")
                {
                    try
                    {
                        KaamsNGCoreService.TopicDetailMst tempList = client.GetTopicDetailById(Convert.ToInt32(topicDetailId));//Convert.ToInt32(encryptId) 
                        List<Topic> relatedTopicList = new List<Topic>();

                        if (tempList != null)
                        {

                            TopicSpecific tempTopic = new TopicSpecific();
                            tempTopic.SMEDetails = tempList.PrimarySME;
                            //tempTopic.PreviewImage = tempList.SmallIcon;
                            tempTopic.Owner = tempList.UserMst.Username;//change by maj need name instead of id
                            tempTopic.Status = tempList.StatusMst.Status;
                            //tempTopic.Level = tempList.LevelMst.Level;
                            tempTopic.Objective = tempList.Objective;

                            tempTopic.ExpiryDate = string.IsNullOrEmpty(Convert.ToString(tempList.ExpiryDate)) ? string.Empty : Convert.ToString(tempList.ExpiryDate);
                            tempTopic.LastModified = string.IsNullOrEmpty(Convert.ToString( tempList.ModifiedDate)) ?  string.Empty :Convert.ToString( tempList.ModifiedDate);
                            tempTopic.DateCreated = string.IsNullOrEmpty(Convert.ToString(tempList.CreatedDate)) ? string.Empty : Convert.ToString(tempList.CreatedDate);
                            tempTopic.ReviewDate = string.IsNullOrEmpty(Convert.ToString(tempList.ReviewDate)) ? string.Empty : Convert.ToString(tempList.ReviewDate);

                            Response<TopicSpecific> response = new Response<TopicSpecific>(tempTopic, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<TopicSpecific>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<TopicSpecific>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<TopicSpecific>(ErrorCodes.REQUIREFIELD_ERROR, "TopicDetailId " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<TopicSpecific>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<TopicSpecific>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        public Response<List<TopicRelatedLinks>> GetRelatedLinksOfTopic(string topicId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (topicId != null && topicId != "")
                {
                    try
                    {
                        KaamsNGCoreService.Link[] tempList = client.GetRelatedLinksOfTopic(Convert.ToInt32(topicId));//Convert.ToInt32(encryptId) 
                        List<TopicRelatedLinks> LinksList = new List<TopicRelatedLinks>();

                        if (tempList.Count() > 0)
                        {


                            foreach (KaamsNGCoreService.Link m in tempList)
                            {
                                TopicRelatedLinks tempLink = new TopicRelatedLinks();
                                tempLink.RelatedLinkId = m.LinkId;
                                tempLink.Title = m.Title;
                                //tempLink.TopicId = m.TopicMst.TopicId;
                                tempLink.URL = m.Url;
                                tempLink.ModifiedDate = m.ModifiedDate;
                                tempLink.CreatedDate = m.CreatedDate;
                                tempLink.Target = m.Target;

                                LinksList.Add(tempLink);

                            }


                            Response<List<TopicRelatedLinks>> response = new Response<List<TopicRelatedLinks>>(LinksList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<TopicRelatedLinks>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<List<TopicRelatedLinks>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<TopicRelatedLinks>>(ErrorCodes.REQUIREFIELD_ERROR, "TopicId " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<TopicRelatedLinks>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<TopicRelatedLinks>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        public Response<List<TopicEvents>> GetEventsOfTopic(string topicId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (topicId != null && topicId != "")
                {
                    try
                    {
                        KaamsNGCoreService.EventInfo[] tempList = client.GetEventsOfTopic(Convert.ToInt32(topicId));//Convert.ToInt32(encryptId) 
                        List<TopicEvents> eventList = new List<TopicEvents>();

                        if (tempList.Count() > 0)
                        {


                            foreach (KaamsNGCoreService.EventInfo m in tempList)
                            {
                                TopicEvents tempEvent = new TopicEvents();
                                tempEvent.EventId = m.eventId;
                                tempEvent.Title = m.Title;
                                tempEvent.Description = m.Description;
                                tempEvent.Owner = m.Owner;
                                tempEvent.Date = m.date;
                                tempEvent.Type = m.Type;

                                eventList.Add(tempEvent);

                            }


                            Response<List<TopicEvents>> response = new Response<List<TopicEvents>>(eventList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<TopicEvents>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<List<TopicEvents>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<TopicEvents>>(ErrorCodes.REQUIREFIELD_ERROR, "TopicId " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<TopicEvents>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<TopicEvents>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        public Response<List<TopicAssociation>> GetTopicAssocation(string topicId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (topicId != null && topicId != "")
                {
                    try
                    {
                        KaamsNGCoreService.SignPost[] tempList = client.GetSignPostOfTopic(Convert.ToInt32(topicId));//Convert.ToInt32(encryptId) 
                        List<TopicAssociation> associationList = new List<TopicAssociation>();

                        if (tempList.Count() > 0)
                        {


                            foreach (KaamsNGCoreService.SignPost m in tempList)
                            {
                                TopicAssociation tempAssoc = new TopicAssociation();
                                tempAssoc.SignPostId = m.SignPostId;
                                tempAssoc.Name = m.Name;
                                tempAssoc.Description = m.Description;
                                associationList.Add(tempAssoc);

                            }


                            Response<List<TopicAssociation>> response = new Response<List<TopicAssociation>>(associationList, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<List<TopicAssociation>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<List<TopicAssociation>>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<List<TopicAssociation>>(ErrorCodes.REQUIREFIELD_ERROR, "TopicId " + RequiredField_Message, validMessageOrNewToken);
                }

            }
            else if (validCode == "2")
            {
                return new Response<List<TopicAssociation>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
            }
            else
            {
                return new Response<List<TopicAssociation>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        public string AddToRecentlyViewed(RecentViewRequest reqRecentlyViewed)
        {
            if (reqRecentlyViewed != null)
            {
                int userId = reqRecentlyViewed.UserId;
                int topicId = reqRecentlyViewed.TopicId;
                string libraryTopicId = Convert.ToString(reqRecentlyViewed.LibraryTopicId);

                try
                {
                    ServiceKaamsNGClient client = new ServiceKaamsNGClient();
                    client.AddToRecentlyViewed(Convert.ToInt32(userId), Convert.ToInt32(topicId), libraryTopicId);
                    return "success";
                }
                catch (Exception ex)
                {
                    return "error";
                }
            }
            else
            {
                return "error";
            }
        }

        /// <summary>
        /// AddRemoveFollowTopic
        /// this will follow/unfollow topic 
        /// </summary>
        /// <param name="reqFollowTopic"></param>
        /// <returns></returns>
        public string AddRemoveFollowTopic(FollowTopicRequest reqFollowTopic)
        {
            if (reqFollowTopic != null)
            {
                int UserId = reqFollowTopic.UserId;
                int TopicId = reqFollowTopic.TopicId;
                int Follow = reqFollowTopic.Follow;

                try
                {
                    ServiceKaamsNGClient client = new ServiceKaamsNGClient();
                    client.AddRemoveFollowingTopics(UserId, TopicId, Follow);
                    return "success";
                }
                catch (Exception ex)
                {
                    return "error";
                }
            }
            else
            {
                return "error";
            }
        }


        public string AddToFavourite(FavoriteList reqFavouriteList)
        {
            if (reqFavouriteList != null)
            {
                int userId = reqFavouriteList.UserId;
                string contentType = reqFavouriteList.ContentType;
                int contentId = Convert.ToInt32(reqFavouriteList.ContentId);
                string title = reqFavouriteList.Title;
                string URL = reqFavouriteList.URL;
                try
                {
                    ServiceKaamsNGClient client = new ServiceKaamsNGClient();
                    client.AddToFavourite(userId, contentType, contentId, title, URL);
                    return "success";
                }
                catch (Exception ex)
                {
                    return "error";
                }
            }
            else
            {
                return "error";
            }
        }

        public string RecommendToUser(UserRecommendMst reqUserRecommend)
        {

            if (reqUserRecommend != null)
            {
                int byUserId = Convert.ToInt32(reqUserRecommend.RecommendedById);
                string toUserId = Convert.ToString(reqUserRecommend.RecommendedToId);
                string title = reqUserRecommend.Title;
                string contentType = Convert.ToString(reqUserRecommend.ContentTypeId);
                decimal contentId = reqUserRecommend.ContentId;

                try
                {
                    ServiceKaamsNGClient client = new ServiceKaamsNGClient();
                    string result = client.RecommendedToUser(Convert.ToInt32(byUserId), toUserId, title, contentType, Convert.ToInt32(contentId));
                    if (result == "")
                    {
                        return "success";
                    }
                    else
                    {
                        return "error";
                    }
                }
                catch (Exception ex)
                {
                    return "error";
                }
            }
            else
            {
                return "input object is null";
            }
        }

        /// <summary>
        /// Create Library
        /// </summary>
        /// <param name="reqLibrary"></param>
        /// <returns></returns>
        public Response<CreateLibrary> CreateLibrary(Request<CreateLibrary> reqLibrary)
        {
            //return null;
            bool flag = false;
            string requiredMsg = "";
            CreateLibrary CreateLibReq = reqLibrary.Data;
            string token = reqLibrary.Token;
            //get fields data
            string createdBy = CreateLibReq.CreatedBy;
            string languageid = CreateLibReq.LanguageId;
            string OrganizationId = CreateLibReq.OrganizationId;
            string Owner = CreateLibReq.OwnerId;
            string Description = CreateLibReq.Description;
            string LibraryTitle = CreateLibReq.LibraryTitle;
            string logo = CreateLibReq.Logo;
            string libTypeId = CreateLibReq.libraryTypeId;
            string SME = CreateLibReq.Sme;
            string Keywords = CreateLibReq.keywords;

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {

                if (CreateLibReq != null)
                {

                    if (createdBy == null || createdBy == "")
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", CreatedBy";

                    }
                    else
                    {
                        try
                        {
                            int tempCreatedBy = Convert.ToInt32(createdBy);
                        }
                        catch (Exception ex)
                        {
                            flag = true;
                            requiredMsg = requiredMsg + ", CreatedBy must be integer (id of user)";
                        }
                    }
                    if (!flag)
                    {
                        client.CreateLibrary(Convert.ToInt32(createdBy), Convert.ToInt32(languageid), Convert.ToInt32(OrganizationId), Convert.ToInt32(createdBy), LibraryTitle, Description, "", logo, Keywords, SME, Convert.ToInt32(libTypeId),0);
                        return new Response<CreateLibrary>(reqLibrary.Data, reqLibrary.Token);
                    }
                    else
                    {
                        return new Response<CreateLibrary>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                    }

                }
                else
                {
                    return new Response<CreateLibrary>(ErrorCodes.REQUIREFIELD_ERROR, "Library object is required.", "");
                }
            }
            else if (validCode == "2")
            {
                return new Response<CreateLibrary>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<CreateLibrary>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        /// <summary>
        /// Create Topic In Library
        /// </summary>
        /// <param name="reqTopic"></param>
        /// <returns></returns>
        public Response<CreateTopic> CreateTopicInLibrary(Request<CreateTopic> reqTopic)
        {
            //return null;
            bool flag = false;
            string requiredMsg = "";
            CreateTopic CreateTopicReq = reqTopic.Data;
            string token = reqTopic.Token;
            string LibraryID = Convert.ToString(reqTopic.Data.LibraryId);

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            DTOTopic objDTOTopic = new DTOTopic();


            if (validCode == "1")
            {

                if (CreateTopicReq != null)
                {

                    if (string.IsNullOrEmpty(LibraryID) || LibraryID == "" || LibraryID == "0")
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", LibraryId";
                    }
                    if (CreateTopicReq.TopicName == null || CreateTopicReq.TopicName == "")
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", TopicName";
                    }
                    if (!flag)
                    {
                        objDTOTopic.libraryId = CreateTopicReq.LibraryId;
                        objDTOTopic.topicTitle = CreateTopicReq.TopicName;
                        //objDTOTopic.smallIcon = CreateTopicReq.SmallIcon;
                        //objDTOTopic.largeIcon = CreateTopicReq.LargeIcon;
                        //objDTOTopic.languageId = CreateTopicReq.LanguageId;
                        objDTOTopic.levelId = Convert.ToInt32(CreateTopicReq.Level);
                        objDTOTopic.keywords = CreateTopicReq.keywords;
                        objDTOTopic.longDescription = CreateTopicReq.LongDescription;
                        objDTOTopic.shortDescription = CreateTopicReq.ShortDescription;
                        //objDTOTopic.primarySME = CreateTopicReq.SMEDetails;
                        objDTOTopic.ownerId = Convert.ToInt32(CreateTopicReq.Owner);
                        objDTOTopic.topicTypeId = 16;//temp
                        objDTOTopic.topicStatusId = 1;
                        client.CreateTopicInLibrary(objDTOTopic);

                        return new Response<CreateTopic>(reqTopic.Data, reqTopic.Token);
                    }
                    else
                    {
                        return new Response<CreateTopic>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                    }

                }
                else
                {
                    return new Response<CreateTopic>(ErrorCodes.REQUIREFIELD_ERROR, "Library object is required.", "");
                }
            }
            else if (validCode == "2")
            {
                return new Response<CreateTopic>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<CreateTopic>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
            //return null;
        }

        /// <summary>
        /// added by MAJ
        /// get user information
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Response<RegisterUser> GetUserInformation(string UserId, string token)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                if (UserId != "" || UserId != null)
                {
                    try
                    {
                        KaamsNGCoreService.UserDetailInfo objUserInfo = client.GetUserInfoForApp(Convert.ToInt32(UserId));//Convert.ToInt32(encryptId) 
                        RegisterUser userInfo = new RegisterUser();
                        if (objUserInfo != null)
                        {
                            userInfo.FirstName = objUserInfo.firstName;
                            userInfo.LastName = objUserInfo.lastName;
                            userInfo.Email = objUserInfo.emailId;
                            userInfo.DisplayName = objUserInfo.displayname;
                            userInfo.MobileNumber = objUserInfo.mobileNo;
                            userInfo.PhoneNo = objUserInfo.phoneNo;
                            userInfo.Username = objUserInfo.userName;
                            Response<RegisterUser> response = new Response<RegisterUser>(userInfo, validMessageOrNewToken);
                            return response;
                        }
                        else
                        {
                            return new Response<RegisterUser>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);//strResult = validMessageOrNewToken;
                        }

                    }
                    catch
                    {
                        return new Response<RegisterUser>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<RegisterUser>(ErrorCodes.REQUIREFIELD_ERROR, "UserId " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<RegisterUser>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<RegisterUser>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

        /// <summary>
        /// added by:MAJ
        /// Purspose: Get token by username
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public string GetTokenByUserName(string UserName)
        {
            if (string.IsNullOrEmpty(UserName))
            {
                return "Username rquired";
            }
            else
            {
                ServiceKaamsNGClient client = new ServiceKaamsNGClient();
                string result = client.GetTokenByUserName(UserName);
                if (result == string.Empty)
                {

                    return "error";
                }
                else
                {
                    return result;
                }
            }
        }

        /// <summary>
        /// added by: MAJ
        /// Purpose: Update User Profile
        /// </summary>
        /// <param name="reqUser"></param>
        /// <returns></returns>
        public Response<RegisterUser> UpdateUserProfile(Request<RegisterUser> reqUser)
        {
            //return null;
            bool flag = false;
            string requiredMsg = "";
            RegisterUser UserData = reqUser.Data;
            string token = reqUser.Token;
            int UserId = reqUser.Data.UserId;

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];

            if (validCode == "1")
            {

                if (UserData != null)
                {

                    if (UserId == 0)
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", UserId";

                    }
                    if (UserData.Username == null || UserData.Username == "")
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", Username";

                    }
                    if (!flag)
                    {
                        if (!client.IsDisplayNameExist(UserData.Username, UserData.DisplayName))
                        {
                            //if username/displayname not exists the allow to update
                            UserDetailInfo objUserDetail = new UserDetailInfo();
                            objUserDetail.firstName = UserData.FirstName;
                            objUserDetail.lastName = UserData.LastName;
                            objUserDetail.kaamsUserId = UserData.UserId;
                            objUserDetail.mobileNo = UserData.MobileNumber;
                            objUserDetail.phoneNo = UserData.PhoneNo;
                            objUserDetail.emailId = UserData.Email;
                            objUserDetail.displayname = UserData.DisplayName;
                            objUserDetail.userName = UserData.Username;
                            client.EditUserProfile(objUserDetail, null);
                            return new Response<RegisterUser>(reqUser.Data, reqUser.Token);
                        }
                        else
                        {
                            return new Response<RegisterUser>(ErrorCodes.USER_EXISTS, "Displayname already exists", "");
                        }

                    }
                    else
                    {
                        return new Response<RegisterUser>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                    }

                }
                else
                {
                    return new Response<RegisterUser>(ErrorCodes.REQUIREFIELD_ERROR, "Library object is required.", "");
                }
            }
            else if (validCode == "2")
            {
                return new Response<RegisterUser>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<RegisterUser>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
            //return null;
        }

        /// <summary>
        /// added by: MAJ
        /// Purpose: Update User Profile
        /// </summary>
        /// <param name="reqUser"></param>
        /// <returns></returns>
        public Response<RegisterUser> RegisterUser(Request<RegisterUser> reqUser)
        {
            //return null;
            bool flag = false;
            string requiredMsg = "";
            RegisterUser userData = reqUser.Data;
            string token = reqUser.Token;
            int userId = reqUser.Data.UserId;
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];

            if (validCode == "1")
            {

                if (userData != null)
                {

                    if (userId == 0)
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", UserId";

                    }
                    if (userData.Username == null || userData.Username == "")
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", Username";

                    }
                    if (!flag)
                    {
                        if (!client.CheckIfUserExists(userData.Username))
                        {
                            //to check if email is already exists
                            string emailverified = client.IsUserActiveForForgotPassword(userData.Email);
                            if (emailverified == "not exists")
                            {
                                UserDetailInfo objUserDetail = new UserDetailInfo();
                                objUserDetail.firstName = userData.FirstName;
                                objUserDetail.lastName = userData.LastName;
                                objUserDetail.kaamsUserId = userData.UserId;
                                objUserDetail.mobileNo = userData.MobileNumber;
                                objUserDetail.phoneNo = userData.PhoneNo;
                                objUserDetail.emailId = userData.Email;
                                objUserDetail.displayname = userData.DisplayName;
                                objUserDetail.userName = userData.Username;
                                objUserDetail.createdDate = DateTime.Now;
                                objUserDetail.modifiedDate = DateTime.Now;
                                objUserDetail.role = "Admin";
                                objUserDetail.password = EncodePassword(userData.Password);
                                objUserDetail.languageId = 1;
                                client.CreateUser(objUserDetail);
                                return new Response<RegisterUser>(reqUser.Data, reqUser.Token);
                            }
                            else
                            {
                                return new Response<RegisterUser>(ErrorCodes.USER_EXISTS, "Invalid email or Already exists", "");
                            }
                        }
                        else
                        {
                            return new Response<RegisterUser>(ErrorCodes.USER_EXISTS, "User name already exists", "");
                        }

                    }
                    else
                    {
                        return new Response<RegisterUser>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                    }

                }
                else
                {
                    return new Response<RegisterUser>(ErrorCodes.REQUIREFIELD_ERROR, "User object is required.", "");
                }
            }
            else if (validCode == "2")
            {
                return new Response<RegisterUser>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<RegisterUser>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }
        }

      
        /// <summary>
        /// EncodePassword
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        private string EncodePassword(string password)
        {
            string encodedPassword = password;
            HMACSHA1 hash = new HMACSHA1();
            ConfigurationManager.RefreshSection("appSettings");
            string validationKey = System.Configuration.ConfigurationManager.AppSettings["validationKey"]; //"BEDA3DD162F2ACC1D8FAB2E6B77BFB2F918773909FED94F82EEDBB12097E1FF4B190397544247BD2EC6C739C8B86F3D57EAE1E5C16A28E7BD88E7EB356401C91";// ConfigurationManager.AppSettings["ValidationKey"];System.Configuration.ConfigurationManager.AppSettings["validationKey"];// VALIDATIONKEY;//
            hash.Key = HexToByte(validationKey);

            encodedPassword =
              Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));

            return encodedPassword;
        }

        /// <summary>
        /// Hex To Byte
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        private byte[] HexToByte(string hexString)
        {
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        /// <summary>
        ///added by: maj
        ///propose: post reply to comment
        /// </summary>
        /// <param name="reqComment"></param>
        /// <returns></returns>
        public Response<Comments> ReplyToComment(Request<Comments> reqComment)
        {
            Comments CommentObject = reqComment.Data;
            if (CommentObject != null)
            {
                string token = reqComment.Token;
                string userID = CommentObject.UserID;
                string topicID = CommentObject.TopicID;
                string commentDetails = CommentObject.CommentDetails;
                string rating = CommentObject.Rating;
                string commentID = CommentObject.CommentID;

                ServiceKaamsNGClient client = new ServiceKaamsNGClient();
                string[] validTokenResult = client.ValidateToken(token);
                string validCode = validTokenResult[0];
                string validMessageOrNewToken = validTokenResult[1];
                if (validCode == "1")
                {
                    bool flag = false;
                    string requiredMsg = "";
                    if (userID == null || userID == "")
                    {
                        flag = true;
                        requiredMsg = "UserID";
                    }
                    if (topicID == null || topicID == "")
                    {
                        flag = true;
                        requiredMsg = requiredMsg + " TopicID";
                    }
                    if (!flag)
                    {
                        try
                        {
                            //call to core webservie to add comment
                            int result = client.AddReplyComments(Convert.ToInt32(commentID), Convert.ToInt32(topicID), Convert.ToInt32(userID), commentDetails, Convert.ToInt32(rating));
                            Response<Comments> response = new Response<Comments>(CommentObject, validMessageOrNewToken);
                            return response;
                        }
                        catch (Exception ex)
                        {
                            return new Response<Comments>(ErrorCodes.GENERIC_ERROR, Generic_Error_Message, validMessageOrNewToken);
                        }
                    }
                    else
                    {
                        return new Response<Comments>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                    }
                }
                else if (validCode == "2")
                {
                    return new Response<Comments>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");
                }
                else
                {
                    return new Response<Comments>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
                }
            }
            else
            {
                return new Response<Comments>(ErrorCodes.NO_DATA, No_Data_Message,reqComment.Token);
            }
        }

        /// <summary>
        /// added by maj:DeleteTopic
        /// 06/08/2014
        /// </summary>
        /// <param name="libraryID"></param>
        /// <param name="topicId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public string DeleteTopic(string libraryID, string topicID, string userID, string token)
        {
            bool flag = false;
            string requiredMsg = "";

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];

            if (validCode == "1")
            {

                if (libraryID == "0" || libraryID == null)
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", libraryId";

                }
                if (topicID == "0" || topicID == null)
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", topicId";
                }
                if (!flag)
                {
                    client.DisassociateTopic(Convert.ToInt32(userID), Convert.ToInt32(topicID), Convert.ToInt32(libraryID));
                    return "Success";
                }
                else
                {
                    return requiredMsg;
                }
            }
            else if (validCode == "2")
            {
                return Token_Expired_Message;
            }
            else
            {
                return "";
            }
           
        }

        /// <summary>
        /// added by maj:Delete artefact
        /// 07/08/2014
        /// </summary>
        /// <param name="atefactDetailID"></param>
        /// <param name="UserID"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public string DeleteArtifact(string atefactDetailID, string TopicDetailId, string UserID, string token)
        {
            bool flag = false;
            string requiredMsg = "";

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];

            if (validCode == "1")
            {
                if (atefactDetailID == "0" || atefactDetailID == null)
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", atefactID";
                }
                if (UserID == "0" || UserID == null)
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", UserID";
                }
                if (!flag)
                {
                    client.DeleteTopicArtefactRelation(Convert.ToInt32(atefactDetailID), Convert.ToInt32(TopicDetailId) ,Convert.ToInt32(UserID));
                    return "Success";
                }
                else
                {
                    return requiredMsg;
                }
                //return "Success";
            }
            else if (validCode == "2")
            {
                return Token_Expired_Message;
            }
            else
            {
                return Generic_Error_Message;
            }
        }

        /// <summary>
        /// added by maj:DeleteLibrary
        /// 07/08/2014
        /// </summary>
        /// <param name="libraryID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string DeleteLibrary(string libraryID, string userID, string token)
        {
            bool flag = false;
            string requiredMsg = "";

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];

            if (validCode == "1")
            {
                if (libraryID == "0" || libraryID == null)
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", atefactID";
                }
                if (userID == "0" || userID == null)
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", UserID";
                }
                if (!flag)
                {
                    int result = client.DeleteLibraryMessage(Convert.ToInt32(libraryID), Convert.ToInt32(userID));
                    if (result == 1)//topics under library are not associated with any other library
                    {
                        return "0";//if library is has topics which are associated with other libraries
                    }
                    else
                    {
                        client.DeleteLibrary(Convert.ToInt32(libraryID), Convert.ToInt32(userID));
                        return "Success";
                    }
                    //return "Success";
                }
                else
                {
                    return requiredMsg;
                }
              
            }
            else if (validCode == "2")
            {
                return Token_Expired_Message;
            }
            else
            {
                return Generic_Error_Message;
            }
        }
    }
}
