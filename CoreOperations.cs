﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Data.SqlClient;

namespace KaamsAppRestService
{
    public class CoreOperations
    {
        // Use in iphone/ipad the has function CC_SHA512(keyData.bytes, keyData.length, digest);

        #region Security - data Hassshing
        public string WSHashString512(string text)
        {
            string hexString = String.Empty;
            try
            {
                System.Text.ASCIIEncoding UE = new System.Text.ASCIIEncoding();
                string secretCode = "secretCode";
                String TextWithRegex = System.Text.RegularExpressions.Regex.Replace(text, "[^a-zA-Z0-9]", String.Empty);
                string textFull = TextWithRegex + secretCode;
                byte[] hashValue;
                byte[] message = UE.GetBytes(textFull);

                SHA512Managed hashString = new SHA512Managed();
                hashValue = hashString.ComputeHash(message);
                foreach (byte x in hashValue)
                { hexString += String.Format("{0:x2}", x); }
            }
            catch (Exception e)
            {
                WSLoggerDebug(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString(), "Exception", "exception: " + e.Message + "<br/>" + e.StackTrace + "<br/>");
                return null;
            }
            return hexString;
        }

        #endregion

        #region DEBUG LOGS
        //DEBUG LOGS Function!

        public Response<bool> WSLoggerDebug(string functionName, string typeDebugLog, string description)
        {
            if (String.IsNullOrEmpty(functionName))
            {
                functionName = "N/A";
            }
            if (String.IsNullOrEmpty(typeDebugLog))
            {
                typeDebugLog = "Debug";
            }
            if (String.IsNullOrEmpty(description))
            {
                description = "-";
            }
            string IpClient = System.Web.HttpContext.Current.Request.UserHostAddress;


            //SqlConnection connection = new SqlConnection(CONNECTION_STRING);

            //using (connection)
            //{
            //    connection.Open();
            //    SqlCommand command = new SqlCommand();
            //    command.Connection = connection;
            //    command.CommandText = "Insert INTO Debug_LOGS " +
            //                              "(NomeFunzione,Tipo,Descrizione,IpClient) " +
            //                          "VALUES " +
            //                              "('" + NomeFunzione.ToString() + "','" + TipoDebugLog.ToString() + "','" + Descrizione.ToString().Replace("'", "") + "','" + IpClient.ToString() + "')";
            //    int result = command.ExecuteNonQuery();
            int result=1;
                if (result > 0)
                { return new Response<bool>(true); }
                else { return new Response<bool>(false); }
            
        }
        #endregion
    }
}