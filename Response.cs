﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace KaamsAppRestService
{


    public enum ErrorCodes
    {
        NONE = 200,
        GENERIC_ERROR = 1,
        EXCEPTION = 2,
        PARTIAL_ERROR = 3,
        REQUIREFIELD_ERROR = 3,
        DB_GENERIC_ERROR = 10,
        INSERT_FAILED = 11,
        UPDATE_FAILED = 12,
        USER_NOT_FOUND = 100,
        USER_ALREADY_REGISTERED = 101,
        USER_OR_PASSWORD_WRONG = 102,
        TOKEN_EXPIRED = 103,
        TOKEN_NOT_PROPER = 104,
        DELETION_FAILED = 203,
        NO_DATA = 400,
        UPLOAD_FAILED = 500,
        USER_EXISTS = 105

    }

    [DataContract]
    public class Response<T>
    {
        private ErrorCodes errorCode;
        [DataMember(Name = "ErrorCode")]
        public ErrorCodes ErrorCode
        {
            get { return this.errorCode; }
            set { this.errorCode = value; }
        }

        private string errorMessage;
        [DataMember(Name = "ErrorMessage")]
        public string ErrorMessage
        {
            get { return this.errorMessage; }
            set { this.errorMessage = value; }
        }

        private T data;
        [DataMember(Name = "Data")]
        public T Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        private string token;
        [DataMember(Name = "Token")]
        public string Token
        {
            get { return this.token; }
            set { this.token = value; }
        }

        public Response()
        {
        }

        public Response(ErrorCodes code, string message, string token)
        {
            this.ErrorCode = code;
            this.ErrorMessage = message;
            this.Token = token;
        }

        public Response(T data, string token)
        {
            this.ErrorCode = ErrorCodes.NONE;
            this.Data = data;
            this.Token = token;
        }
    }

    [DataContract]
    public class CashPoints
    {

        /*[DataMember]
        public string TotalCashPoint { get; set; }
        [DataMember]
        public string UserFavorite { get; set; }
        [DataMember]
        public string UserView { get; set; }
        [DataMember]
        public string UserRecommend { get; set; }
        [DataMember]
        public string UserFollow { get; set; }
        [DataMember]
        public string OwnerCreate { get; set; }
        [DataMember]
        public string OwnerRecommend { get; set; }
        [DataMember]
        public string OwnerFollow { get; set; }
        [DataMember]
        public string OwnerView { get; set; }
        [DataMember]
        public string OwnerFavorite { get; set; }*/
        [DataMember]
        public decimal Points { get; set; }
        [DataMember]
        public string Type { get; set; }


    }

    [DataContract]
    public class NotificationList
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string Userfolder = System.Configuration.ConfigurationManager.AppSettings["Userfolder"];

        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string ContentTitle { get; set; }
        [DataMember]
        public DateTime? Date { get; set; }
        [DataMember]
        public string FromUser { get; set; }
        [DataMember]
        public decimal Id { get; set; }
        string profile_Picture = "";
        [DataMember]
        public string ProfilePicture
        {

            get
            {
                return profile_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    profile_Picture = ROOT_URL + "/" + Userfolder + "no-image.png";
                }
                else
                {
                    profile_Picture = ROOT_URL + "/" + Userfolder + value;
                }

            }
        }
        [DataMember]
        public decimal RecommendedId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ToUser { get; set; }
        [DataMember]
        public bool? Viewed { get; set; }
    }

    [DataContract]
    public class UserRecommendMst
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string Userfolder = System.Configuration.ConfigurationManager.AppSettings["Userfolder"];

        [DataMember]
        public decimal RecommendId { get; set; }

        [DataMember]
        public string ContentTypeName { get; set; }
        [DataMember]
        public decimal ContentTypeId { get; set; }

        [DataMember]
        public decimal ContentId { get; set; }

        [DataMember]
        public string RecommendTitle { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        [DataMember]
        public decimal RecommendedById { get; set; }
        [DataMember]
        public string RecommendedByName { get; set; }

        [DataMember]
        public decimal RecommendedToId { get; set; }
        [DataMember]
        public string RecommendedToName { get; set; }

        [DataMember]
        string profile_Picture = "";
        [DataMember]
        public string RecommendedByProfilePicture
        {

            get
            {
                return profile_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    profile_Picture = ROOT_URL + "/" + Userfolder + "no-image.png";
                }
                else
                {
                    profile_Picture = ROOT_URL + "/" + Userfolder + value;
                }
            }
        }

        [DataMember]
        public decimal RecommendedId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public bool? Viewed { get; set; }

        [DataMember]
        public int LanguageId { get; set; }

        [DataMember]
        public Topic TopicDetails { get; set; }
    }

    [DataContract]
    public class FavoriteList
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];

        [DataMember]
        public string ContentTypeName { get; set; }
        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public decimal ContentId { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? ModifiedDate { get; set; }
        [DataMember]
        public decimal FavoriteId { get; set; }


        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string URL { get; set; }

        [DataMember]
        public int LanguageId { get; set; }

        [DataMember]
        public Topic TopicDetails { get; set; }

        [DataMember]
        public int UserId { get; set; }

    }

    [DataContract]
    public class RecentViewList
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];

        [DataMember]
        public string ContentTypeName { get; set; }
        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public decimal ContentId { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? ModifiedDate { get; set; }
        [DataMember]
        public decimal RecentViewId { get; set; }


        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string URL { get; set; }

        [DataMember]
        public int LanguageId { get; set; }

        [DataMember]
        public Topic TopicDetails { get; set; }

        

    }

    // this contract is used for request object of addtorecentlyviewed.
    [DataContract]
    public class RecentViewRequest
    {

        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int TopicId { get; set; }
        [DataMember]
        public int LibraryTopicId { get; set; }

    }

    //this contract is used for request object of Follow/Unfollow topic
    //maj 09/07/2014
    [DataContract]
    public class FollowTopicRequest
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int TopicId { get; set; }
        [DataMember]
        public int Follow { get; set; }
    }

    [DataContract]
    public class TopicRelatedTopic
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string Topic_Icons = System.Configuration.ConfigurationManager.AppSettings["topicicons"];
        [DataMember]
        public decimal RelatedTopicsId { get; set; }
        [DataMember]
        public decimal TopicDetailId { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        public string Review_Date = "";
        public string Expiry_Date = "";
        string created_date = "";
        [DataMember]
        public string CreatedDate {
            get
            {
                return created_date;
            }
            set
            {
                if (value == null)
                {

                    created_date = "";
                }
                else
                {
                    created_date = value;
                }

            }
        
        }

        string modified_date = "";
        [DataMember]
        public string ModifiedDate
        {

            get
            {
                return modified_date;
            }
            set
            {
                if (value == null)
                {

                    modified_date = "";
                }
                else
                {
                    modified_date = value;
                }

            }
        }


        [DataMember]
        public int LanguageId { get; set; }

        string preview_Picture = "";
        [DataMember]
        public string PreviewImage
        {

            get
            {
                return preview_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains("."))
                {

                    preview_Picture = ROOT_URL + "/" + Topic_Icons + "preview_Image.jpg";
                }
                else
                {
                    preview_Picture = ROOT_URL + "/" + Topic_Icons + value;
                }

            }
        }

        [DataMember]
        public string ShortDescription { get; set; }
        [DataMember]
        public string LongDescription { get; set; }
        [DataMember]
        public string DateCreated { get; set; }
        [DataMember]
        public string SMEDetails { get; set; }
        [DataMember]
        public int Likes { get; set; }
        [DataMember]
        public int CommentsCount { get; set; }
        [DataMember]
        public decimal Owner { get; set; }
        [DataMember]
        public string Objective { get; set; }
        //made datefield nullable as it was containing garbage value
        [DataMember]
        public string ExpiryDate
        {
            get
            {
                return Expiry_Date;
            }
            set
            {
                if (value == null)
                {

                    Expiry_Date = "";
                }
                else
                {
                    Expiry_Date = value;
                }

            }

        }
        [DataMember]
        public string ReviewDate
        {
            get
            {
                return Review_Date;
            }
            set
            {
                if (value == null)
                {

                    Review_Date = "";
                }
                else
                {
                    Review_Date = value;


                }
            }
        }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Level { get; set; }

        [DataMember]
        public int LibTopicId { get; set; }
        [DataMember]
        public Topic TopicDetails { get; set; }
        [DataMember]
        public decimal liberaryId { get; set; }
        public string SmallIcon { get; set; }
        public string LargeIcon { get; set; }
       
    }

    [DataContract]
    public class TopicRelatedLinks
    {

        [DataMember]
        public decimal RelatedLinkId { get; set; }
        [DataMember]
        public decimal TopicId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Target { get; set; }
        [DataMember]
        public string URL { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? ModifiedDate { get; set; }
        [DataMember]
        public bool IsImport { get; set; }


    }

    [DataContract]
    public class TopicEvents
    {

        [DataMember]
        public string Owner { get; set; }
        [DataMember]
        public decimal EventId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public decimal TopicId { get; set; }

    }

    [DataContract]
    public class TopicAssociation
    {

        [DataMember]
        public decimal SignPostId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? ModifiedDate { get; set; }

    }

    [DataContract]
    public class RegisterUser
    {
         [DataMember]
        public string FirstName { get; set; }
         [DataMember]
        public string LastName { get; set; }
         [DataMember]
        public string Email { get; set; }
         [DataMember]
        public string Username { get; set; }
         [DataMember]
        public string Password { get; set; }
         [DataMember]
        public string DisplayName { get; set; }
         [DataMember]
        public string PhoneNo { get; set; }
         [DataMember]
        public string MobileNumber { get; set; }
         [DataMember]
         public int UserId { get; set; }
       
        
    }

}