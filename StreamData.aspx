﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StreamData.aspx.cs" Inherits="StreamData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <script src="jquery-1.7.1.min.js" type="text/javascript"></script>

    <title></title>
    <script type="text/javascript">
        var xhr = null;
        var startTime = null;

        function progress(e) {
            if (e.lengthComputable) {
                $('progress').attr({ value: e.loaded });
                $('#progress').text(Math.round((e.loaded / e.total) * 100));
                var loadedKB = e.loaded / 1024000;
                var totalKB = e.total / 1024000;
                $('#trData').text(loadedKB.toFixed(2) + '/' + totalKB.toFixed(2));
                var timeDiff = (new Date().getTime() - startTime) / 1000;
                $('#uSpeed').text((loadedKB / timeDiff).toFixed(2));
            }
        };

        function complete(e) {
            if (this.status == 200) {
                var r = jQuery.parseJSON(this.response);
                $("#status").html(r);
            } else {
                // error
                $("#status").html(this.response);
            }
            xhr = null;
            startTime = null;
            $('input[type="file"]').removeAttr("disabled");
            $("#cancel").attr("disabled", "disabled");
        };

        function error(e) {
            xhr = null;
            startTime = null;
            $('input[type="file"]').removeAttr("disabled");
            $("#cancel").attr("disabled", "disabled");
            $("#status").html(this.response);
        }


        $(document).ready(function () {


            $('input[type="file"]').change(function () {
                $("#status").html("");
                $('#progress').text("");
                $('#uSpeed').text("");
                $('#trData').text("");
                var file = this.files[0];
                $('progress').attr({ value: 0, max: file.size });
                $(this).attr("disabled", "disabled");
                xhr = new XMLHttpRequest();

                xhr.upload.onprogress = progress;
                xhr.upload.onloadend = complete;
                xhr.upload.onerror = error;

                xhr.open('POST', 'http://localhost:1812/StreamedService.svc/UploadStreamWithMessageContract', true);
                xhr.setRequestHeader("Accept", "application/json, text/javascript, */*; q=0.01");
                startTime = new Date().getTime();

              
                var UploadStreamMessage = {
                    FileName: file.name,
                    Content: file
                };


                xhr.send(UploadStreamMessage);
                $("#cancel").removeAttr("disabled");
            });

            $("#cancel").click(function () {
                xhr.abort();
                xhr = null;
                $('input[type="file"]').removeAttr("disabled");
                $("#cancel").attr("disabled", "disabled");
                $("#status").html("Upload canceled by user.");
            });


        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <input type="file" />
<br /><br />
<progress value="0" max="100" style="width:100%; height:20px;"></progress>
<br />
<table>
    <tr>
        <td>Progress</td>
        <td id="progress">0</td>
        <td>%</td>
    </tr>
    <tr>
        <td>Trasfered data</td>
        <td id="trData">0</td>
        <td>KB</td>
    </tr>
    <tr>
        <td>Upload Speed</td>
        <td id="uSpeed">0</td>
        <td>bytes/sec</td>
    </tr>
</table>
<input id="cancel" type="button" value="Cancel Upload" disabled="disabled" />
<br />
<span id="status" style="color:green; font-weight:bold;"></span>

    </div>
   
    </form>
</body>
</html>
