﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace KaamsAppRestService
{
    [DataContract]
    public class User
    {
        private  string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string USER_FOLDER = System.Configuration.ConfigurationManager.AppSettings["Userfolder"];

        [DataMember]
        public string UserID { get; set; }
        [DataMember]
        public string UserFullName { get; set; }
        [DataMember]
        public string EmailID { get; set; }
        [DataMember]
        public string UserType { get; set; }
        [DataMember]
        public string Location { get; set; }

        string profile_Picture = "";
        [DataMember]
        public string ProfilePicture { 
           
             get
            {
                return profile_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    profile_Picture = ROOT_URL + "/" + USER_FOLDER + "/no-image.png";
                }
                else
                {
                    profile_Picture = ROOT_URL + "/" + USER_FOLDER+ value;
                }
           
            }
        }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string BundleId { get; set; }
     
    }
}