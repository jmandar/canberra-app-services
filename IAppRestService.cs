﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;

namespace KaamsAppRestService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAppRestService" in both code and config file together.
    [ServiceContract]
    public interface IAppRestService
    {
        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.WrappedRequest,
               UriTemplate = "GetLibraryList/{userId}/{token}")]
        Response<List<Library>> GetLibraryList(string userId,string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.WrappedRequest,
               UriTemplate = "GetLibraryData/{libId}/{token}")]
        Response<List<Topic>> GetLibraryData(string libId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.WrappedRequest,
               UriTemplate = "GetTopicData/{topicDetailId}/{token}")]
        Response<TopicWithArtefact> GetTopicData(string topicDetailId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.WrappedRequest,
               UriTemplate = "GetLibraryTopicAndArtefactData/{libId}/{token}")]
        Response<List<TopicWithArtefact>> GetLibraryTopicAndArtefactData(string libId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.WrappedRequest,
               UriTemplate = "GetCommentsData/{topicid}/{token}")]
        Response<List<Comments>> GetCommentsData(string topicid, string token);


        [OperationContract]
        [WebInvoke(
               Method = "POST",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "LoginUser")]
        Response<User> LoginUser(User requestLogin);

        [OperationContract]
        [WebInvoke(
               Method = "POST",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "PostComment")]
        Response<List<Comments>> PostComment(Request<Comments> comment);

        [OperationContract]
        [WebInvoke(
               Method = "POST",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "EditComment")]
        Response<List<Comments>> EditComment(Request<Comments> comment);

        [OperationContract]
        [WebInvoke(
               Method = "POST",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "DeleteComment")]
        Response<List<Comments>> DeleteComment(Request<Comments> comment);

        [OperationContract]
        [WebInvoke(
               Method = "POST",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "PostRating")]
        Response<Rating> PostRating(Request<Rating> ratingJson);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "CreateArtefact")]
        Response<List<Artefact>> CreateArtefact(Request<Artefact> artefact);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetCashPointsOfUser/{userId}/{token}")]
        Response<List<CashPoints>> GetCashPointsOfUser(string userId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetUserNotification/{userId}/{token}")]
         Response<List<NotificationList>> GetUserNotification(string userId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetRecommendedListForUser/{userId}/{languageId}/{token}")]
        Response<List<UserRecommendMst>> GetRecommendedListForUser(string userId,string languageId, string token);


        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetUserFavorite/{userId}/{languageId}/{token}")]
        Response<List<FavoriteList>> GetUserFavorite(string userId,string languageId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetUserRecentView/{userId}/{languageId}/{token}")]
        Response<List<RecentViewList>> GetUserRecentView(string userId,string languageId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetTopicRelatedTopics/{topicDetailId}/{token}")]
        Response<List<TopicRelatedTopic>> GetTopicRelatedTopics(string topicDetailId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetTopicInfoByTopicDetailId/{topicDetailId}/{token}")]
        Response<TopicSpecific> GetTopicInfoByTopicDetailId(string topicDetailId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetRelatedLinksOfTopic/{topicId}/{token}")]
        Response<List<TopicRelatedLinks>> GetRelatedLinksOfTopic(string topicId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetEventsOfTopic/{topicId}/{token}")]
        Response<List<TopicEvents>> GetEventsOfTopic(string topicId, string token);

        [OperationContract]
        [WebInvoke(
               Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare,
               UriTemplate = "GetTopicAssocation/{topicId}/{token}")]
        Response<List<TopicAssociation>> GetTopicAssocation(string topicId, string token);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "AddToRecentlyViewed")]
        string AddToRecentlyViewed(RecentViewRequest reqRecentlyViewed);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "AddRemoveFollowTopic")]
        string AddRemoveFollowTopic(FollowTopicRequest reqFollowTopic);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "AddToFavourite")]
        string AddToFavourite(FavoriteList reqFavoritelist);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "RecommendToUser")]
       string RecommendToUser(UserRecommendMst reqUserRecommend);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "CreateLibrary")]
        Response<CreateLibrary> CreateLibrary(Request<CreateLibrary> library);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "CreateTopicInLibrary")]
        Response<CreateTopic> CreateTopicInLibrary(Request<CreateTopic> topics);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "GetTokenByUserName/{UserName}")]
        string GetTokenByUserName(string UserName);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "GetUserInformation/{UserId}/{token}")]
        Response<RegisterUser> GetUserInformation(string UserId, string token);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "UpdateUserProfile")]
        Response<RegisterUser> UpdateUserProfile(Request<RegisterUser> reqUser);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "RegisterUser")]
        Response<RegisterUser> RegisterUser(Request<RegisterUser> reqUser);
        
        //modifications on 07/08/2014

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "DeleteLibrary/{libraryID}/{userID}/{token}")]
        string DeleteLibrary(string libraryID, string userID, string token);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "DeleteArtifact/{atefactDetailID}/{TopicDetailId}/{UserID}/{token}")]
        string DeleteArtifact(string atefactDetailID, string TopicDetailId, string UserID, string token);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "DeleteTopic/{libraryID}/{topicID}/{userID}/{token}")]
        string DeleteTopic(string libraryID, string topicID, string userID, string token);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "ReplyToComment")]
        Response<Comments> ReplyToComment(Request<Comments> reqComment);
    }

}

