﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace KaamsAppRestService
{
    public partial class TestFunctionality : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void postComment_Click(object sender, EventArgs e)
        {

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (flUpload.HasFile)
            {
                AppRestService ap = new AppRestService();

                string username = HttpUtility.UrlEncode("prabhu");
                string password = HttpUtility.UrlEncode("pppppp");
                string bundleId = HttpUtility.UrlEncode("12*3");
                KaamsAppRestService.User requestLogin = new KaamsAppRestService.User();
                requestLogin.UserName = username;
                requestLogin.Password = password;
                requestLogin.BundleId = bundleId;

                KaamsAppRestService.Response<User> R = ap.LoginUser(requestLogin);
                string token = R.Token;
                Request<Artefact> requestObject = new Request<Artefact>();
                requestObject.Token = token;
                Artefact requestArtefact = new Artefact();
                requestArtefact.TopicDetailID="17";
                requestArtefact.ArtifactName="AApp Uploaded Artefact";
                requestArtefact.Description="This is App Uploaded Artefact";
                requestArtefact.OwnerID="23";
                requestArtefact.LanguageID="1";
                requestArtefact.ArtefactTypeID="1";
                requestArtefact.FileName=flUpload.FileName;
                requestArtefact.Version="1";
                requestArtefact.ExpiryDate=DateTime.Now.AddYears(10).ToString();
                requestArtefact.SmallIcon="";
                requestArtefact.LargeIcon="";
                requestArtefact.KeyWords="App service upload";
                requestArtefact.Transcript="";
                requestArtefact.CreatedBy="23";
                requestArtefact.ArtefactStatusID = "1";
                //requestArtefact.ContentType = flUpload.PostedFile.ContentType;

                int fileLen;

                // Get the length of the file.
                fileLen = flUpload.PostedFile.ContentLength;
                // Create a byte array to hold the contents of the file.
                byte[] input = new byte[fileLen - 1];
                input = flUpload.FileBytes;
                requestArtefact.ArtefactContent = Convert.ToBase64String(input);
                WriteTextFile(Convert.ToBase64String(input));
                requestObject.Data = requestArtefact;

                hdnContent.Value = Convert.ToBase64String(input);
                hdnContentType.Value = flUpload.PostedFile.ContentType;
                hdnFileName.Value = flUpload.PostedFile.FileName;
                hdnToken.Value = token;

                //KaamsAppRestService.Response<List<Artefact>> artArray = ap.UploadArtefact(requestObject);

                KaamsAppRestService.StreamedService client = new StreamedService();
                UploadStreamMessage upstremMsg = new UploadStreamMessage();
                upstremMsg.ContentType = flUpload.PostedFile.ContentType;
                upstremMsg.fileName = flUpload.PostedFile.FileName;
                upstremMsg.Content = flUpload.FileContent;

                KaamsAppRestService.FileToUpload fl = new FileToUpload();
                fl.Content = Convert.ToBase64String(input);
                fl.FileName = flUpload.PostedFile.FileName;

               // client.UploadStreamWithMessageContract(upstremMsg);


               
            }
 
            
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                AppRestService ap = new AppRestService();
                int fileLen;
                // Get the length of the file.
                fileLen = FileUpload1.PostedFile.ContentLength;
                // Create a byte array to hold the contents of the file.
                byte[] input = new byte[fileLen - 1];
                input = FileUpload1.FileBytes;
                string base64StringContent = Convert.ToBase64String(input);
                string url = "http://localhost:50881/Content/Documents/artefact";//"http://192.168.43.131:82/members";//
                //string result = ap.HttpPost(url, base64StringContent);
            }
        }

        private void WriteTextFile(string content)
        {
            string filePath = Server.MapPath("~/Test.txt");
            FileStream aFile = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            StreamWriter sw = new StreamWriter(aFile);
            sw.Write(content);
            sw.Close();
            aFile.Close();
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (fluUploadFile.HasFile)
            {
                AppRestService ap = new AppRestService();
                int fileLen;
                // Get the length of the file.
                fileLen = fluUploadFile.PostedFile.ContentLength;
                // Create a byte array to hold the contents of the file.
                byte[] input = new byte[fileLen - 1];
                input = fluUploadFile.FileBytes;
                string base64StringContent = Convert.ToBase64String(input);
                FileToUpload fltemp = new FileToUpload();
                fltemp.Content = base64StringContent;
                fltemp.FileName = fluUploadFile.PostedFile.FileName;
                fltemp.ContentType = fluUploadFile.PostedFile.ContentType;
                //Response<FileToUpload> flToUp = ap.UploadFile(fltemp);
                //Response.Write(flToUp.Data.Content);

            }
        }
    }
}