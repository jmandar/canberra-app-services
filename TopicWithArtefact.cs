﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace KaamsAppRestService
{
    [DataContract]
    public class TopicWithArtefact
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string Topic_Icons = System.Configuration.ConfigurationManager.AppSettings["topicicons"];
       
        
        string topic_id = "0";
        [DataMember]
        public string TopicID {
            get
            {
                return topic_id;
            }
            set
            {
                topic_id = value;

            }
        }

        string topic_detail_id = "0";
        [DataMember]
        public string TopicDetailID
        {
            get
            {
                return topic_detail_id;
            }
            set
            {
                topic_detail_id = value;

            }
        }

        string topic_name = "";
        [DataMember]
        public string TopicName
        {
            get
            {
                return topic_name;
            }
            set
            {
                topic_name = value;

            }
        }

        string preview_Picture = "";
        [DataMember]
        public string PreviewImage
        {

            get
            {
                return preview_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    preview_Picture = ROOT_URL + "/" + Topic_Icons + "preview_Image.jpg";
                }
                else
                {
                    preview_Picture = ROOT_URL + "/" + Topic_Icons + value;
                }

            }
        }

        string short_description = "";
        [DataMember]
        public string ShortDescription
        {
            get
            {
                return short_description;
            }
            set
            {
                short_description = value;

            }
        }

        string long_description = "";
        [DataMember]
        public string LongDescription
        {
            get
            {
                return long_description;
            }
            set
            {
                long_description = value;

            }
        }

        [DataMember]
        public string DateCreated { get; set; }
        [DataMember]
        public string SMEDetails { get; set; }
        [DataMember]
        public int Likes { get; set; }
        [DataMember]
        public int CommentsCount { get; set; }
        [DataMember]
        public decimal Owner { get; set; }
        [DataMember]
        public string Objective { get; set; }

        [DataMember]
        public DateTime? ExpiryDate { get; set; }
        [DataMember]
        public DateTime? ReviewDate { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Level { get; set; }
        [DataMember]
        public List<Artefact> ArtefactList { get; set; }

        [DataMember]
        public int LibTopicId { get; set; }

        [DataMember]
        public int ArtefactCount { get; set; }
        [DataMember]
        public int ParentId;
    }
}