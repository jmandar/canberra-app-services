﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace KaamsAppRestService
{
    [DataContract]
    public class Request<T>
    {
        private T data;
        [DataMember(Name = "Data")]
        public T Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        private string token;
        [DataMember(Name = "Token")]
        public string Token
        {
            get { return this.token; }
            set { this.token = value; }
        }
        public Request(T data, string token)
        {
           
            this.Data = data;
            this.Token = token;
        }
        public Request()
        {

            this.Data = data;
            this.Token = token;
        }
    }


   
}