﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace KaamsAppRestService
{
    [DataContract]
    public class FileToUpload
    {
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string ContentType { get; set; }
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string FileType { get; set; } //1:Video, 2:Pdf, 3:Doc, 4:Ppt, 5:Xls, 6:Audio,7:Image,8:Text. Required.

    }

    
}