﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using KaamsAppRestService.KaamsNGCoreService;
using System.Data;
using System.IO;
using System.Web;
using System.Net;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Globalization;


namespace KaamsAppRestService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "StreamedService" in code, svc and config file together.  [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    
    public class StreamedService : IStreamData
    {
        public static string sRegSpecialCharforGoal = "[^a-zA-Z0-9._!&-]";
        public static string sRegSpecialChar = "[^a-zA-Z0-9._@!&-]";
        public static string curPath = "";

        private const string RequiredField_Message = "is required.";
        private const string Generic_Error_Message = "Generic Error.";
        private const string Token_Expired_Code = "103";
        private const string Token_Expired_Message = "Token Expired. Please login again to get new token.";
        private const string No_Data_Message = "No Data";

        public string DoWork()
        {
            return "";
        }
        /*  public string UploadFile(Stream fileContents)
          {
              byte[] buffer = new byte[100000];
              int bytesRead;
              long totalBytesRead = 0;
      
              try
              {
                  do
                  {
                      bytesRead = fileContents.Read(buffer, 0, buffer.Length);
                      totalBytesRead += bytesRead;
                      System.Diagnostics.Trace.WriteLine("totalBytesRead: " + totalBytesRead);
                  } while (bytesRead > 0);
              }
              catch (Exception ex)
              {
                  // THIS IS NOT SOMETHING YOU WANT TO DO IN A REAL WORLD APPLICATION
                  return ("Error: " + ex.Message);
              }

              return (String.Format("Total bytes read for file {0}: {1}", "demo.txt", totalBytesRead));
          }
          */

        /// <summary>
        /// modified by maj
        /// purpose: post file to webportal in base64 format
        /// </summary>
        /// <param name="fileToUpload"></param>
        /// <returns></returns>
        public Response<FileToUpload> UploadFile(FileToUpload fileToUpload)
        {
            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string token = fileToUpload.Token;
            string filename = fileToUpload.FileName;
            string content = fileToUpload.Content;
            string contentType = fileToUpload.ContentType;
            string fileType = fileToUpload.FileType;

            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
         
            if (validCode == "1")
            {
                bool flag = false;
                string requiredMsg = "";
                if (filename == null || filename == "")
                {
                    flag = true;
                    requiredMsg = "Filename";
                }
                else
                {
                    filename = GetFileNameHavingOriginalaName(filename);
                }
                if (content == null || content == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + " FileContent";
                }

                if (!flag)
                {

                    try
                    {
                        string result = PostFile(content, filename, contentType,fileType);
                        if (result.ToLower() == "false")
                        {
                            return new Response<FileToUpload>(ErrorCodes.UPLOAD_FAILED, "File upload Error", validMessageOrNewToken);
                        }
                        else
                        {
                            fileToUpload.Content = "";
                            fileToUpload.FileName = filename;
                            fileToUpload.FileType = fileType;
                            //fileToUpload.Token = validMessageOrNewToken;

                            Response<FileToUpload> response = new Response<FileToUpload>(fileToUpload, validMessageOrNewToken);
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {

                        FileToUpload fault = new FileToUpload();
                        fault.FileName = ex.Message;
                        fault.Content = ex.ToString();
                        //throw new FaultException<FileToUpload>(fault, ex.ToString());

                        return new Response<FileToUpload>(fault, validMessageOrNewToken);//Response<FileToUpload>(ErrorCodes.UPLOAD_FAILED, "File upload Error", validMessageOrNewToken);
                    }
                }
                else
                {
                    return new Response<FileToUpload>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                }
            }
            else if (validCode == "2")
            {
                return new Response<FileToUpload>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<FileToUpload>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }


        }

        public static string GetFileNameHavingOriginalaName(string fileName)
        {
            fileName = GetUploadFileNameStatic(fileName);
            fileName = Regex.Replace(fileName, sRegSpecialCharforGoal, "");
            return Path.GetFileNameWithoutExtension(fileName) + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + System.IO.Path.GetExtension(fileName);
        }
        //Static Version of GetUploadFileName 
        public static string GetUploadFileNameStatic(string sFilename)
        {
            if (sFilename == "")
                return sFilename;
            FileInfo fi = new FileInfo(sFilename);
            string sWithoutExtension = Path.GetFileNameWithoutExtension(sFilename);
            string sExtension = fi.Extension;
            if (sWithoutExtension.Length > 15)
                sFilename = sWithoutExtension.Substring(0, 15);
            else
                sFilename = sWithoutExtension;
            sFilename = RemoveSpecialChars(sFilename);
            return sFilename + sExtension;
        }

        public static string RemoveSpecialChars(string fileName)
        {
            fileName = Regex.Replace(fileName, sRegSpecialChar, "");
            return fileName;
        }

        public string PostFile(string file64Content, string fileName, string contentType,string fileType)
        {
            // Create a request using a URL that can receive a post. 
            string pageWhichUploads = ConfigurationManager.AppSettings["PostDataPage"];
            WebRequest request = WebRequest.Create(pageWhichUploads);
            // Set the Method property of the request to POST.
            request.Method = "POST";
            request.Headers.Add("filename", fileName);
            request.Headers.Add("filetype", fileType);
            request.ContentType = contentType;
            //NetworkCredential netcred = new NetworkCredential();
            //netcred.UserName = "netwin_admin";
            //netcred.Password = "S3cur1ty2013";
            //netcred.Domain = "amshare";
            // Create POST data and convert it to a byte array.
            byte[] byteArray = Convert.FromBase64String(file64Content); //Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.

            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.

            //Console.WriteLine(responseFromServer);

            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();
            return responseFromServer;
        }

        public string UploadStream(Stream fileContents)
        {
           

            byte[] buffer = new byte[100000];
            int bytesRead;
            long totalBytesRead = 0;

            try
            {
                do
                {
                    bytesRead = fileContents.Read(buffer, 0, buffer.Length);
                    totalBytesRead += bytesRead;
                    System.Diagnostics.Trace.WriteLine("totalBytesRead: " + totalBytesRead);
                } while (bytesRead > 0);
            }
            catch (Exception ex)
            {
                // THIS IS NOT SOMETHING YOU WANT TO DO IN A REAL WORLD APPLICATION
                return ("Error: " + ex.Message);
            }

            return (String.Format("Total bytes read for file {0}: {1}", "Amrutvel", totalBytesRead));
        }

        /*public void UploadStreamWithMessageContract(UploadStreamMessage uploadingStream)
        {
            string fileName = uploadingStream.fileName;
            Stream fileContents = uploadingStream.Content;
            string contentType = uploadingStream.ContentType;

            byte[] buffer = new byte[100000];
            int bytesRead;
            long totalBytesRead = 0;

            do
            {
                bytesRead = fileContents.Read(buffer, 0, buffer.Length);
                totalBytesRead += bytesRead;
                System.Diagnostics.Trace.WriteLine("totalBytesRead: " + totalBytesRead);
            } while (bytesRead > 0);


           

             // Create a request using a URL that can receive a post. 
            string pageWhichUploads = ConfigurationManager.AppSettings["PostDataPage"];
            WebRequest request = WebRequest.Create(pageWhichUploads);
            // Set the Method property of the request to POST.
            request.Method = "POST";
            request.Headers.Add("filename", fileName);
            request.ContentType = contentType;
            //NetworkCredential netcred = new NetworkCredential();
            //netcred.UserName = "netwin_admin";
            //netcred.Password = "S3cur1ty2013";
            //netcred.Domain = "amshare";
            // Create POST data and convert it to a byte array.
            //byte[] byteArray = Convert.FromBase64String(file64Content); //Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = fileContents.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(buffer, 0, Convert.ToInt32(fileContents.Length));
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.

            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.

            //Console.WriteLine(responseFromServer);

            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();
           // return responseFromServer;


          
        }*/

        /// <summary>
        /// modified by maj
        /// purpose: create artifcat and upload file
        /// </summary>
        /// <param name="artefact"></param>
        /// <returns></returns>
        public Response<List<Artefact>> CreateArtefact(Request<Artefact> artefact)
        {
            Artefact requestArtefact = artefact.Data;
            string token = artefact.Token;


            string topicDetailId = requestArtefact.TopicDetailID;
            string artefactTitle = requestArtefact.ArtifactName;
            string description = requestArtefact.Description;
            string ownerId = requestArtefact.OwnerID;

            string languageId = requestArtefact.LanguageID;
            string artefactStatusId = requestArtefact.ArtefactStatusID;
            string artefactTypeId = requestArtefact.ArtefactTypeID;
            string filename = requestArtefact.FileName;
            string version = requestArtefact.Version;
            string expiryDate = requestArtefact.ExpiryDate;
            string smallIcon = requestArtefact.SmallIcon;
            string largeIcon = requestArtefact.LargeIcon;
            string keywords = requestArtefact.KeyWords;
            string transcript = requestArtefact.Transcript;
            string createdBy = requestArtefact.CreatedBy;
            string ownerID = requestArtefact.OwnerID;
            string base64StringContent = requestArtefact.ArtefactContent;
            string contentType = requestArtefact.ContentType;

            string reviewDate = requestArtefact.ReviewDate;

            ServiceKaamsNGClient client = new ServiceKaamsNGClient();
            string[] validTokenResult = client.ValidateToken(token);
            string validCode = validTokenResult[0];
            string validMessageOrNewToken = validTokenResult[1];
            if (validCode == "1")
            {
                bool flag = false;
                string requiredMsg = "";


                if (topicDetailId == null || topicDetailId == "")
                {
                    flag = true;
                    requiredMsg = "TopicDetailID";

                }
                if (createdBy == null || createdBy == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", CreatedBy";
                }
                else
                {
                    try
                    {
                        int tempCreatedBy = Convert.ToInt32(createdBy);
                    }
                    catch (Exception ex)
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", CreatedBy must be integer (id of user)";
                    }
                }
                if (base64StringContent == null || base64StringContent == "")
                {
                    flag = true;
                   requiredMsg = requiredMsg + ", ArtefactContent";
                }
                if (artefactTypeId == null || artefactTypeId == "" || artefactTypeId == "0")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", ArtefactTypeID (It must have values between 1 to 8 for 1:Video, 2:Pdf, 3:Doc, 4:Ppt, 5:Xls, 6:Audio,7:Image,8:Text)";
                }
                if (filename == null || filename == "")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", FileName";
                }
               else
                {
                   filename = GetFileNameHavingOriginalaName(filename);

                }
                if (artefactStatusId == null || artefactStatusId == "" || artefactStatusId == "0")
                {
                    flag = true;
                    requiredMsg = requiredMsg + ", ArtefactStatusID (It must have values between 1 to 4)";
                }

                if (expiryDate == null || expiryDate == "")
                {
                    expiryDate = DateTime.Now.AddYears(20).ToShortDateString();
                }
                else
                {
                    try
                    {
                        DateTime tempDate = DateTime.ParseExact(expiryDate.Trim(), "d/M/yyyy", CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", Expiry date must be in format (dd/mm/yyyy)";

                    }
                }
                if (reviewDate == null || reviewDate == "")
                {
                    reviewDate = DateTime.Now.AddYears(20).ToShortDateString();
                }
                else
                {
                    try
                    {
                        DateTime tempDate = DateTime.ParseExact(reviewDate.Trim(), "d/M/yyyy", CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        flag = true;
                        requiredMsg = requiredMsg + ", Expiry date must be in format (dd/mm/yyyy)";

                    }
                }


                if (flag)
                {
                    return new Response<List<Artefact>>(ErrorCodes.REQUIREFIELD_ERROR, requiredMsg + " " + RequiredField_Message, validMessageOrNewToken);
                }
                else
                {
                    try
                    {
                        DTOArtefact dtoArtefact = new DTOArtefact();

                        dtoArtefact.ArtefactTitle = artefactTitle;
                        dtoArtefact.ArtefactTypeId = Convert.ToInt32(artefactTypeId);
                        dtoArtefact.Base64ArtefactContent = base64StringContent;
                        dtoArtefact.CreatedBy = Convert.ToInt32(createdBy);
                        dtoArtefact.Description = description;
                        dtoArtefact.ExpiryDate = expiryDate;
                        dtoArtefact.FileName = filename;
                        dtoArtefact.Keywords = keywords;
                        dtoArtefact.LanguageId = 1;
                        dtoArtefact.LargeIcon = largeIcon;
                        dtoArtefact.SmallIcon = "";
                        dtoArtefact.OwnerId = Convert.ToInt32(ownerID);
                        dtoArtefact.TopicDetailID = Convert.ToInt32(topicDetailId);
                        dtoArtefact.Transcript = transcript;
                        dtoArtefact.Version = version;
                        dtoArtefact.ReviewDate = reviewDate;
                        //dtoArtefact.ArtefactStatusId = 1;


                        string uploadResponse = PostFile(base64StringContent, filename, contentType, artefactTypeId);

                        if (uploadResponse != null && uploadResponse.ToLower() == "true")
                        {

                        dtoArtefact.ArtefactStatusId = Convert.ToInt32(artefactStatusId);
                        ArtefactListInfo[] tempArtList = client.UploadArtefact(dtoArtefact);

                        List<Artefact> arttList = new List<Artefact>();
                        if (tempArtList.Count() > 0)
                        {


                            foreach (ArtefactListInfo m in tempArtList)
                            {
                                Artefact temp = new Artefact();
                                temp.ArtifactID = Convert.ToString(m.ArtefactId);
                                temp.ArtifactName = m.Title;
                                temp.PreviewImage = m.largeIcon;
                                //temp.ArtefactTypeID = Convert.ToString(m.ArtefactTypeId);
                                temp.ArtifactURL = m.filePath;
                                temp.CommentsCount = 0;
                                temp.DateCreated = Convert.ToString(m.createdDate);
                                temp.Description = m.Description;
                                temp.Likes = 5;
                                temp.SMEDetails = "";

                                arttList.Add(temp);
                            }


                            Response<List<Artefact>> response = new Response<List<Artefact>>(arttList, validMessageOrNewToken);
                            return response;

                        }
                        else
                        {
                            return new Response<List<Artefact>>(ErrorCodes.NO_DATA, No_Data_Message, validMessageOrNewToken);
                        }
                        }
                        else
                        {
                        return new Response<List<Artefact>>(ErrorCodes.UPLOAD_FAILED, "File upload Error", validMessageOrNewToken);
                        }
                    }
                    catch (Exception ex)
                    {
                        return new Response<List<Artefact>>(ErrorCodes.GENERIC_ERROR, "Generic Error", validMessageOrNewToken);
                    }
                }
            }
            else if (validCode == "2")
            {
                return new Response<List<Artefact>>(ErrorCodes.TOKEN_EXPIRED, Token_Expired_Message, "");

            }
            else
            {
                return new Response<List<Artefact>>(ErrorCodes.TOKEN_NOT_PROPER, Token_Expired_Message, "");
            }


        }



    }
}