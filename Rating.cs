﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace KaamsAppRestService
{
    [DataContract]
    public class Rating
    {
        [DataMember]
        public string TopicID { get; set; }
        [DataMember]
        public string AverageRating { get; set; }
        [DataMember]
        public string RatingNumber { get; set; }
        [DataMember]
        public string CommentRatingID { get; set; }
        [DataMember]
        public string UserID { get; set; }
        [DataMember]
        public string CommentText { get; set; }

    }
}