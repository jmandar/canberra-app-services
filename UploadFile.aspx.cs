﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using KaamsAppRestService;

public partial class UploadFile : System.Web.UI.Page
{
    public static string sRegSpecialCharforGoal = "[^a-zA-Z0-9._!&-]";
    public static string sRegSpecialChar = "[^a-zA-Z0-9._@!&-]";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        lblMsg.Text = string.Empty;

        string docType = string.Empty;
        string path = string.Empty;

        if (FileUpLoad1.PostedFile.FileName != string.Empty)
        {
            int fileLen;

            // Get the length of the file.
            fileLen = FileUpLoad1.PostedFile.ContentLength;

            // Display the length of the file in a label.
            lblMsg.Text = "The length of the file is "
                               + fileLen.ToString() + " bytes.";

            // Create a byte array to hold the contents of the file.
            byte[] input = new byte[fileLen - 1];
            input = FileUpLoad1.FileBytes;

            // Copy the byte array to a string.
            //for (int loop1 = 0; loop1 < fileLen; loop1++)
            //{
            //    displayString = displayString + input[loop1].ToString();
            //}
            KaamsAppRestService.AppRestService ka = new AppRestService();
            
         
            lblMsg.Text = "File Uploaded Successfully.";
        }
        else
        {
            lblMsg.Text = "Please select file to upload.";
        }
    }

    public static string GetFileNameHavingOriginalaName(string fileName)
    {
        fileName = GetUploadFileNameStatic(fileName);
        fileName = Regex.Replace(fileName, sRegSpecialCharforGoal, "");
        return System.IO.Path.GetFileName(fileName) + "@" + System.DateTime.Now.Day + System.DateTime.Now.Month
            + System.DateTime.Now.Year + System.DateTime.Now.Hour + System.DateTime.Now.Minute
            + System.DateTime.Now.Second + System.IO.Path.GetExtension(fileName);
    }

    //Static Version of GetUploadFileName 
    public static string GetUploadFileNameStatic(string sFilename)
    {
        if (sFilename == "")
            return sFilename;
        FileInfo fi = new FileInfo(sFilename);
        string sWithoutExtension = Path.GetFileNameWithoutExtension(sFilename);
        string sExtension = fi.Extension;
        if (sWithoutExtension.Length > 15)
            sFilename = sWithoutExtension.Substring(0, 15);
        else
            sFilename = sWithoutExtension;
        sFilename = RemoveSpecialChars(sFilename);
        return sFilename + sExtension;
    }

    public static string RemoveSpecialChars(string fileName)
    {
        fileName = Regex.Replace(fileName, sRegSpecialChar, "");
        return fileName;
    }
}