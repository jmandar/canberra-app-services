﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace KaamsAppRestService
{
    [DataContract]
    public class Artefact
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string ARTEFACT_ICONS = System.Configuration.ConfigurationManager.AppSettings["artefacticons"];
        private string Artefact_Video_Normal = System.Configuration.ConfigurationManager.AppSettings["artefactvideonormal"];
        private string Artefact_Audio = System.Configuration.ConfigurationManager.AppSettings["artefactaudio"];
        private string Artefact_Video_High = System.Configuration.ConfigurationManager.AppSettings["artefactvideohigh"];
        private string Artefact_Images = System.Configuration.ConfigurationManager.AppSettings["artefactimages"];
        private string Artefact_Documents = System.Configuration.ConfigurationManager.AppSettings["artefactdocuments"];
        

        [DataMember]
        public string ArtifactID { get; set; }
        [DataMember]
        public string ArtifactName { get; set; }

        string preview_Picture = "";
        [DataMember]
        public string PreviewImage
        {

            get
            {
                return preview_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    preview_Picture = ROOT_URL + "/"+ARTEFACT_ICONS+"preview_Image.jpg";
                }
                else
                {
                    preview_Picture = ROOT_URL + "/" + ARTEFACT_ICONS + value;
                }

            }
        }

       
       

        // filepath,filename
        string artefact_Url = "";
        [DataMember]
        public string ArtifactURL
        {

            get
            {
                return artefact_Url;
            }
            set
            {
               
                if (value == null || value == "" || !value.Contains("."))
                {
                    if (this.ArtefactTypeID == null || this.ArtefactTypeID == "")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + "preview_Image.jpg";
                    }
                    else if (this.ArtefactTypeID == "1")//video
                    {
                        artefact_Url = ROOT_URL + "/"+Artefact_Video_Normal+"Preview_video.jpg";
                    }
                    else if (this.ArtefactTypeID == "2")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + "Preview_Pdf.jpg";
                    }
                    else if (this.ArtefactTypeID == "3")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + "Preview_Word.jpg";
                    }
                    else if (this.ArtefactTypeID == "4")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + "Preview_PowerPoint.jpg";
                    }
                    else if (this.ArtefactTypeID == "5")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + "Preview_Excel.jpg";
                    }
                    else if (this.ArtefactTypeID == "6")//audio
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Audio + "preview_Audio.jpg";
                    }
                    else if (this.ArtefactTypeID == "7")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Images + "preview_Image.jpg";
                    }
                    else if (this.ArtefactTypeID == "8")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + "Preview_txt.jpg";
                    }
                }
                else
                {

                    if (this.ArtefactTypeID == null || this.ArtefactTypeID == "")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + value;
                    }
                    else if (this.ArtefactTypeID == "1")//video
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Video_Normal + value;
                    }
                    else if (this.ArtefactTypeID == "2")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + value;
                    }
                    else if (this.ArtefactTypeID == "3")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + value;
                    }
                    else if (this.ArtefactTypeID == "4")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + value;
                    }
                    else if (this.ArtefactTypeID == "5")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + value;
                    }
                    else if (this.ArtefactTypeID == "6")//audio
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Audio + value;
                    }
                    else if (this.ArtefactTypeID == "7")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Images + value;
                    }
                    else if (this.ArtefactTypeID == "8")
                    {
                        artefact_Url = ROOT_URL + "/" + Artefact_Documents + value;
                    }
                }


            }
        }


        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DateCreated { get; set; }
        [DataMember]
        public string SMEDetails { get; set; }
        [DataMember]
        public int Likes { get; set; }
        [DataMember]
        public int CommentsCount { get; set; }
        [DataMember]
        public string UserID { get; set; }
        [DataMember]
        public string LanguageID { get; set; }
        [DataMember]
        public string ArtefactStatusID { get; set; }
        [DataMember]
        public string ArtefactType { get; set; }
        [DataMember]
        public string ArtefactTypeID { get; set; }
        [DataMember]
        public string Version { get; set; }
        [DataMember]
        public string ExpiryDate { get; set; }
        [DataMember]
        public string ReviewDate { get; set; }
        [DataMember]
        public string KeyWords { get; set; }
        [DataMember]
        public string Transcript { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string ArtefactContent { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string TopicID { get; set; }
        [DataMember]
        public string TopicDetailID { get; set; }
        [DataMember]
        public string OwnerID { get; set; }
        [DataMember]
        public string SmallIcon { get; set; }
        string LargeIconTemp = "";
        [DataMember]
        public string LargeIcon
        {
            get
            {
                return LargeIconTemp;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) 
                {
                    if (this.ArtefactTypeID == null || this.ArtefactTypeID == "")
                    {
                        LargeIconTemp = StaticInfo.Preview_Image_Image;
                    }
                    else if (this.ArtefactTypeID == "1")//video
                    {
                        LargeIconTemp = StaticInfo.Preview_Image_video;
                    }
                    else if (this.ArtefactTypeID == "2")
                    {
                        LargeIconTemp = StaticInfo.Preview_Image_Pdf;
                    }
                    else if (this.ArtefactTypeID == "3")
                    {
                        LargeIconTemp =  StaticInfo.Preview_Image_Word;
                    }
                    else if (this.ArtefactTypeID == "4")
                    {
                        LargeIconTemp = StaticInfo.Preview_Image_PPT;
                    }
                    else if (this.ArtefactTypeID == "5")
                    {
                        LargeIconTemp =  StaticInfo.Preview_Image_Excel;
                    }
                    else if (this.ArtefactTypeID == "6")//audio
                    {
                        LargeIconTemp =  StaticInfo.Preview_Image_Audio;
                    }
                    else if (this.ArtefactTypeID == "7")
                    {
                        LargeIconTemp =  StaticInfo.Preview_Image_Image;
                    }
                    else if (this.ArtefactTypeID == "8")
                    {
                        LargeIconTemp =  StaticInfo.Preview_Image_Text;
                    }
                    //LargeIconTemp = ROOT_URL + "/" + ARTEFACT_ICONS + "preview_Image.jpg";
                }
                else
                {
                    LargeIconTemp = ROOT_URL + "/" + ARTEFACT_ICONS + value;
                }

            }
        }
         [DataMember]
         public string ContentType { get; set; }
    }
}