﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace KaamsAppRestService
{
    [DataContract]
    public class Library
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string Library_Folder = System.Configuration.ConfigurationManager.AppSettings["libraryfolder"];

        [DataMember]
        public string LibraryID { get; set; }
        [DataMember]
        public string LibraryDetailID { get; set; }
        [DataMember]
        public string LibraryName { get; set; }

       
        string preview_Picture = "";
        [DataMember]
        public string PreviewImage
        {

            get
            {
                return preview_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    preview_Picture = ROOT_URL + "/" + Library_Folder + "preview_Image.jpg";
                }
                else
                {
                    preview_Picture = ROOT_URL +"/" + Library_Folder +value;
                }

            }
        }

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string TopicsCount { get; set; }
        [DataMember]
        public string DateCreated { get; set; }
     
    }

    [DataContract]
    public class CreateLibrary
    {
      
        [DataMember]
        public int LibraryID { get; set; }
        [DataMember]
        public int LibraryDetailID { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string TopicsCount { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LanguageId { get; set; }
        [DataMember]
        public string OrganizationId { get; set; }
        [DataMember]
        public string OwnerId { get; set; }
        [DataMember]
        public string LibraryTitle { get; set; }
     
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public string keywords { get; set; }
        [DataMember]
        public string Sme { get; set; }
        [DataMember]
        public string libraryTypeId { get; set; }
        

    }
}
