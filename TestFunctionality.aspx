﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestFunctionality.aspx.cs" Inherits="KaamsAppRestService.TestFunctionality" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="jquery-1.7.1.min.js" type="text/javascript"></script>
    <title></title>
    <style>
        .next_function
        {
            clear:both;
            width:100%;
            float:left;
            margin:20px 0px;    
        }
    </style>
    <script type="text/javascript">
        function MadeHttpServiceCall() {

            var RequestObject = new Object();

            RequestObject.Token = $("#hdnToken").val();

            var ArtefactObject = new Object();
            ArtefactObject.TopicDetailID = "17";
            ArtefactObject.ArtifactName = "app folder";
            ArtefactObject.Description = "folder";
            ArtefactObject.OwnerID = "23";
            ArtefactObject.LanguageID = "1";
            ArtefactObject.ArtefactTypeID = "25";
          
            ArtefactObject.Version = "1";
            ArtefactObject.ExpiryDate = "3/2/2024";
            ArtefactObject.KeyWords = "test";
            ArtefactObject.CreatedBy = "23";
            ArtefactObject.ArtefactStatusID = "1";
            ArtefactObject.ArtefactContent = $("#hdnContent").val();
            ArtefactObject.FileName = $("#hdnFileName").val();
            ArtefactObject.ContentType = $("#hdnContentType").val();

            RequestObject.Data = ArtefactObject;

            $.ajax({
                url: 'http://localhost:4147/AppRestService.svc/UploadArtefact',
                data: { artefact: RequestObject },
                cache: false,
                type: "POST",
                async: false,
                data: JSON.stringify(RequestObject),
                contentType: "application/json",
                dataType: "json",
                success: function () {
                    ShowRecorder();
                    $("#lblMessage").text("Audio artefact created succussfully.")
                },
                error: function (xhr, status, error) {
                    alert(status + ":" + error + ":" + xhr.responseText);

                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="hdnToken" />
        <asp:HiddenField runat="server" ID="hdnContent" />
          <asp:HiddenField runat="server" ID="hdnFileName" />
           <asp:HiddenField runat="server" ID="hdnContentType" />
    <div>
    <div class="next_function">
    PostComment: <asp:Button runat="server" Text="Post Comment" ID="postComment" 
            onclick="postComment_Click" />
    </div>
    <div class="next_function">
    Upload Artefact: 
    <asp:FileUpload runat="server" ID="flUpload" />
    <asp:Button runat="server" Text="Upload Artefact" ID="btnUpload" onclick="btnUpload_Click" 
             />
    </div>
    <div class="next_function">
    Post To Other Url : 
    <asp:FileUpload runat="server" ID="FileUpload1" />
    <asp:Button runat="server" Text="Post Artefact" ID="btnPost" onclick="btnPost_Click" 
             />
    </div>
    </div>
    <div onclick="MadeHttpServiceCall()" >Made Call</div>

      <div class="next_function">
    Upload File : 
    <asp:FileUpload runat="server" ID="fluUploadFile" />
    <asp:Button runat="server" Text="Upload File" ID="btnUploadFile" onclick="btnUploadFile_Click" 
             />
    </div>
    </form>
</body>
</html>
