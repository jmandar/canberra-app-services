﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;

namespace KaamsAppRestService
{
    [ServiceContract]
   
   public interface IStreamData
    {
        [OperationContract]
        [WebInvoke(Method = "GET")]
        string DoWork();

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "UploadFile")]
        //[FaultContract(typeof(FileToUpload))]
        Response<FileToUpload> UploadFile(FileToUpload fileToUpload);

        [OperationContract]
        [WebInvoke(UriTemplate = "UploadStream", Method = "POST")]
        string UploadStream(Stream fileContents);

       /* [OperationContract]
        [WebInvoke(UriTemplate = "UploadStreamWithMessageContract", Method = "POST")]
        void UploadStreamWithMessageContract(UploadStreamMessage uploadingStream);*/

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "CreateArtefact")]
        Response<List<Artefact>> CreateArtefact(Request<Artefact> artefact);
    }

    [MessageContract]
    public class UploadStreamMessage: IDisposable
    {
        [MessageHeader(MustUnderstand = true)]
        public string fileName;

        [MessageHeader(MustUnderstand = true)]
        public string ContentType;

        [MessageBodyMember(Order = 1)]
        public Stream Content;
      
        public void Dispose()
        {
            if (Content != null)
            {
                Content.Close();
                Content = null;
            }
        }   
    } 
}
