﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace KaamsAppRestService
{
    [DataContract]
    public class Topic
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string Topic_Icons = System.Configuration.ConfigurationManager.AppSettings["topicicons"];

        [DataMember]
        public string TopicID { get; set; }
        [DataMember]
        public string TopicDetailID { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        string preview_Picture = "";
        [DataMember]
        public string PreviewImage
        {

            get
            {
                return preview_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    preview_Picture = ROOT_URL + "/" + Topic_Icons + "preview_Image.jpg";
                }
                else
                {
                    preview_Picture = ROOT_URL + "/" + Topic_Icons + value;
                }

            }
        }

        [DataMember]
        public string ShortDescription { get; set; }
        [DataMember]
        public string LongDescription { get; set; }
        [DataMember]
        public string DateCreated { get; set; }
        [DataMember]
        public string SMEDetails { get; set; }
        [DataMember]
        public int Likes { get; set; }
        [DataMember]
        public int CommentsCount{ get; set; }
        [DataMember]
        public decimal Owner { get; set; }
        [DataMember]
        public string Objective { get; set; }
        //made datefield nullable as it was containing garbage value
        [DataMember]
        public DateTime? ExpiryDate { get; set; }
        [DataMember]
        public DateTime? ReviewDate { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Level { get; set; }

        [DataMember]
        public int LibTopicId { get; set; }

        [DataMember]
        public int ArtefactCount { get; set; }
        [DataMember]
        public int LibraryId { get; set; }
    }

    [DataContract]
    public class TopicSpecific
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string Topic_Icons = System.Configuration.ConfigurationManager.AppSettings["topicicons"];

        [DataMember]
        public string DateCreated { get; set; }
        [DataMember]
        public string SMEDetails { get; set; }
        [DataMember]
        public string Owner { get; set; }//changed by maj 08-july-2014
        [DataMember]
        public string Objective { get; set; }
        [DataMember]
        //modified datetime to string app needs empty instead of null
        public string ExpiryDate { get; set; }
        [DataMember]
        public string ReviewDate { get; set; }
        public string LastModified { get; set; }
        [DataMember]
        public string Status { get; set; }
        //[DataMember]
        //public string Type { get; set; }
        //[DataMember]
        //public string Level { get; set; }

        //string preview_Picture = "";
        //[DataMember]
        //public string PreviewImage
        //{

        //    get
        //    {
        //        return preview_Picture;
        //    }
        //    set
        //    {
        //        if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
        //        {

        //            preview_Picture = ROOT_URL + "/" + Topic_Icons + "preview_Image.jpg";
        //        }
        //        else
        //        {
        //            preview_Picture = ROOT_URL + "/" + Topic_Icons + value;
        //        }

        //    }
        //}

    }
    //added by maj for new webservice method createtopic in library
    [DataContract]
    public class CreateTopic
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string Topic_Icons = System.Configuration.ConfigurationManager.AppSettings["topicicons"];

        [DataMember]
        public string TopicID { get; set; }
        [DataMember]
        public string TopicDetailID { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        string preview_Picture = "";
        [DataMember]
        public string PreviewImage
        {

            get
            {
                return preview_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    preview_Picture = ROOT_URL + "/" + Topic_Icons + "preview_Image.jpg";
                }
                else
                {
                    preview_Picture = ROOT_URL + "/" + Topic_Icons + value;
                }

            }
        }

        [DataMember]
        public string ShortDescription { get; set; }
        [DataMember]
        public string LongDescription { get; set; }
        [DataMember]
        public string DateCreated { get; set; }

        [DataMember]
        public decimal Owner { get; set; }
       
        //made datefield nullable as it was containing garbage value
        [DataMember]
        public DateTime? ExpiryDate { get; set; }
        [DataMember]
        public DateTime? ReviewDate { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Level { get; set; }

        [DataMember]
        public int LibraryId { get; set; }

        [DataMember]
        public string keywords { get; set; }

      
    }

}