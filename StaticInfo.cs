﻿//added by maj
//purpose:store static variables
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KaamsAppRestService
{
    public class StaticInfo
    {
        public static string Preview_Image_Audio = "preview_Audio.jpg";
        public static string Preview_Image_video = "preview_Video.jpg";
        public static string Preview_Image_Word = "Preview_Word.jpg";
        public static string Preview_Image_Image = "preview_Image.jpg";
        public static string Preview_Image_PPT = "Preview_PowerPoint.jpg";
        public static string Preview_Image_Text = "Preview_txt.jpg";
        public static string Preview_Image_Excel = "Preview_Excel.jpg";
        public static string Preview_Image_Pdf = "Preview_Pdf.jpg";
    }
}