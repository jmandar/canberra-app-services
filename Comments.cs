﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace KaamsAppRestService
{
    [DataContract]
    public class Comments
    {
        private string ROOT_URL = System.Configuration.ConfigurationManager.AppSettings["RootURL"];
        private string USER_FOLDER = System.Configuration.ConfigurationManager.AppSettings["Userfolder"];

        [DataMember]
        public string CommentID { get; set; }
        [DataMember]
        public string CommentDetails { get; set; }
        [DataMember]
        public string CommentedBy { get; set; }
        [DataMember]
        public string Time { get; set; }
        [DataMember]
        public string TopicID { get; set; }
        [DataMember]
        public string UserID { get; set; }
        string profile_Picture = "";
        [DataMember]
        public string UserProfilePicture
        {

            get
            {
                return profile_Picture;
            }
            set
            {
                if (value == null || value == "" || !value.Contains(".")) // if less than zero debit account by 10
                {

                    profile_Picture = ROOT_URL + "/" + USER_FOLDER + "/no-image.png";
                }
                else
                {
                    profile_Picture = ROOT_URL + "/" + USER_FOLDER + value;
                }

            }
        }
        [DataMember]
        public string Rating { get; set; }
     
    }
}