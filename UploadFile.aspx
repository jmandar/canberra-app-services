﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="UploadFile.aspx.cs" Inherits="UploadFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="jquery-1.7.1.min.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        //To check valid file
        function checkFileExtension(elem) {
            var filePath = elem.value;
            if (filePath.indexOf('.') == -1)
                return false;

            var validExtensions = new Array();
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

            validExtensions[0] = 'jpg';
            validExtensions[1] = 'jpeg';
            validExtensions[2] = 'bmp';
            validExtensions[3] = 'png';
            validExtensions[4] = 'gif';
            validExtensions[5] = 'htm';
            validExtensions[6] = 'html';
            validExtensions[7] = 'mp4';
            validExtensions[8] = 'pdf';

            for (var i = 0; i < validExtensions.length; i++) {
                if (ext == validExtensions[i]) {
                    document.getElementById("<%=regexp.ClientID %>").style.display = "none";
                    return true;
                }
            }

            alert('The file extension ' + ext.toUpperCase() + ' is not allowed! \n\nApplicable file formats are:\nVideo: mp4\nDocument: pdf, html, htm\nImage: jpg, png, gif, bmp');
            elem.value = "";

            document.getElementById("<%=regexp.ClientID %>").style.display = "";

            document.getElementById("uploadFile_div").innerHTML =
                    document.getElementById("uploadFile_div").innerHTML;
            return false;
        }

        $(document).ready(function () {

       


        var xhr = null;
        var startTime = null;

        function progress(e) {
            if (e.lengthComputable) {
                $('progress').attr({ value: e.loaded });
                $('#progress').text(Math.round((e.loaded / e.total) * 100));
                var loadedKB = e.loaded / 1024000;
                var totalKB = e.total / 1024000;
                $('#trData').text(loadedKB.toFixed(2) + '/' + totalKB.toFixed(2));
                var timeDiff = (new Date().getTime() - startTime) / 1000;
                $('#uSpeed').text((loadedKB / timeDiff).toFixed(2));
            }
        };

        function complete(e) {
            if (this.status == 200) {
                var r = jQuery.parseJSON(this.response);
                $("#status").html(r);
            } else {
                // error
                $("#status").html(this.response);
            }
            xhr = null;
            startTime = null;
            $('input[type="file"]').removeAttr("disabled");
            $("#cancel").attr("disabled", "disabled");
        };

        function error(e) {
            xhr = null;
            startTime = null;
            $('input[type="file"]').removeAttr("disabled");
            $("#cancel").attr("disabled", "disabled");
            $("#status").html(this.response);
        }

        $("#cancel").click(function () {
            xhr.abort();
            xhr = null;
            $('input[type="file"]').removeAttr("disabled");
            $("#cancel").attr("disabled", "disabled");
            $("#status").html("Upload canceled by user.");
        });

        $('input[type="file"]').change(function () {
            $("#status").html("");
            $('#progress').text("");
            $('#uSpeed').text("");
            $('#trData').text("");
            var file = this.files[0];
            $('progress').attr({ value: 0, max: file.size });
            $(this).attr("disabled", "disabled");
            xhr = new XMLHttpRequest();

            xhr.upload.onprogress = progress;
            xhr.upload.onloadend = complete;
            xhr.upload.onerror = error;

            xhr.open('POST', 'http://localhost:2138/KaamsAppRestService/StreamedService.svc/UploadFile', true);
            xhr.setRequestHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            startTime = new Date().getTime();
            xhr.send(file);
            $("#cancel").removeAttr("disabled");
        });

    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <table style="border: 1px solid gray;" width="100%">
<tr>
<td align="center">
    <p>
        <asp:Label runat="server" ID="lblMsg" ForeColor="Green"></asp:Label>
        <br />
        <div id="uploadFile_div">
            <asp:FileUpload runat="server" ID="FileUpLoad1" onchange="return checkFileExtension(this);" />
        </div>
        <asp:RequiredFieldValidator runat="server" ForeColor="Red" ValidationGroup="FileUpLoad"
            ID="reqFile" ControlToValidate="FileUpLoad1" ErrorMessage="File is required"
            Display="Dynamic">
        </asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="regexp" Font-Size="13px" ForeColor="Red" runat="server"
            Display="Dynamic" SetFocusOnError="true" ValidationGroup="FileUpLoad" ControlToValidate="FileUpLoad1"
            ValidationExpression="^.+\.(([jJ][pP][eE]?[gG])|([gG][iI][fF])|([bB][mM][pP­])|([pP][nN][gG])|([hH][tT][mM][lL]?)|([pP][dD][fF])|([mM][pP][4]))$"><b>Invalid file format</b>
        </asp:RegularExpressionValidator><br />
        <span style="font-size: 13px; font-weight: bold;">(Note: Applicable file formats are:<br />
            Video : mp4, flv<br />
            Document: pdf, html, htm<br />
            Image : jpg, png, gif, bmp)</span>
        <br />
        <br />
        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click"
            CssClass="button" />
    </p>
    </td>
    </tr>
</table>

<input type="file" />
<br /><br />
<progress value="0" max="100" style="width:100%; height:20px;"></progress>
<br />
<table>
    <tr>
        <td>Progress</td>
        <td id="progress">0</td>
        <td>%</td>
    </tr>
    <tr>
        <td>Trasfered data</td>
        <td id="trData">0</td>
        <td>KB</td>
    </tr>
    <tr>
        <td>Upload Speed</td>
        <td id="uSpeed">0</td>
        <td>bytes/sec</td>
    </tr>
</table>
<input id="cancel" type="button" value="Cancel Upload" disabled="disabled" />
<br />
<span id="status" style="color:green; font-weight:bold;"></span>

</asp:Content>
